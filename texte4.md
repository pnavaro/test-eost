# Recommandations sur la certification des services requis pour mettre en application les principes FAIR au sein de l\'EOSC 

## 

## https://op.europa.eu/en/publication-detail/-/publication/70aa74b5-53bf-11eb-b59f-01aa75ed71a1/language-en/format-PDF/source-184009543 

## RÉSUMÉ 

Le rapport contient une analyse des activités relatives à la
certification des services requis pour aboutir à des résultats de
recherche FAIR au sein de l\'EOSC à partir de novembre 2020. Il traite
de l\'incitation et du soutien, propose une analyse des lacunes et des
possibilités d\'extension, et définit les priorités pour les travaux
futurs.

Le groupe de travail FAIR du Bureau exécutif de l\'EOSC a été
initialement chargé de définir l\'approche de certification qui sera
appliquée au sein de l\'EOSC pour les entrepôts qui permettent
d\'obtenir des résultats de recherche FAIR. Nous avons décidé
d\'étendre la portée de notre travail à d\'autres services, en raison
du besoin reconnu de définir des mécanismes de certification pour
d\'autres éléments de l\'écosystème FAIR.

Des travaux sont en cours au sein de FAIRsFAIR pour développer des
systèmes de certification FAIR pour les entrepôts, par l\'extension de
la certification actuelle des entrepôts de données \"de base\" avec
l\'identification des niveaux de maturité des capacités nécessaires
pour soutenir les données qui sont évaluées comme FAIR. CoreTrustSeal
travaille également à étendre son cadre de certification à d\'autres
groupes d\'acteurs. Des réseaux et des projets tels que WDS et CLARIN
utilisent CoreTrustSeal dans leurs processus d\'accréditation, tandis
qu\'ELIXIR a défini ses propres procédés pour répondre à ses besoins.
Parmi les autres contributions, citons les principes TRUST, qui
fournissent un cadre général pour la certification des entrepôts; les
caractéristiques souhaitables des entrepôts dignes de confiance
énumérés dans une demande de commentaires publics de l\'Office of
Science and Technology Policies des États-Unis ; et le cadre utile
pour les meilleures pratiques en matière d'entrepôts publié par la
Confederation of Open Access Repositories en octobre 2020.

Les composants essentiels permettant la mise en place de l\'écosystème
FAIR sont présentés dans la figure ci-dessous (Figure 6 de *Turning
FAIR into Reality - TFiR*). Les briques élémentaires essentielles sur
lesquelles nous nous appuyons pour nos opérations doivent être
certifiées. L\'évaluation de la certification des services mettant en
application les principes FAIR progresse, en particulier avec la
définition par FAIRsFAIR des principes généraux à suivre, et le
travail en cours dans ce projet sur la définition d\'un cadre de base.
Les services à certifier en priorité incluent les services
d'identifiants pérennes (PID) ; le groupe de travail sur
l\'architecture de l\'EOSC a défini un ensemble de sous-thématiques
pour ce processus de certification. Des registres de composants
certifiés doivent être développés comme indiqué sur la figure.

![](media/image1.png){width="2.3687423447069116in"
height="2.6420833333333333in"}

Cloud of registries : Nuage de registres

Policies : Politiques

DMPs : DMP

Identifiers : Identifiants

Standards : Normes

Repositories : Entrepôts

*Turning FAIR into Reality* relève que des actions sont nécessaires
pour favoriser et soutenir les ressources existantes dans leur
progression vers une certification. L\'incitation peut se faire à
différents niveaux, notamment par le biais de politiques et de
projets. L\'aide peut également être apportée de différentes manières,
et inclure un soutien pour améliorer les pratiques de gestion des
données.

Les préoccupations de la communauté en matière d'indicateurs et de
certification ont été exprimées sans équivoque lors de la consultation
sur l\'agenda stratégique pour la recherche et l'innovation (SRIA) de
l\'EOSC qui s\'est tenue durant l\'été 2020, reflétant le besoin
d\'inclusion et le niveau de préparation inégal des services et des
communautés. La période de transition recommandée par TFiR pour
permettre aux entrepôts existants de franchir les étapes nécessaires
pour obtenir la certification devrait être autorisée. La certification
comme les indicateurs ne doivent pas être une méthode punitive, et
elles ne doivent pas être utilisées pour des comparaisons entre
entrepôts ou champs disciplinaires.

**À ce stade, en raison de la nécessité d\'être inclusif et des
différents stades de préparation des communautés et de leurs services,
le statut de certification ne peut pas être une condition nécessaire
pour qu\'un entrepôt ou d\'autres composants clés soient inclus dans
l\'EOSC.**

**À un moment donné, la certification pourrait devenir une condition
préalable à l\'inclusion dans l'EOSC, en particulier pour les
entrepôts de données mais aussi pour d\'autres éléments clés de
l'EOSC. Cette décision ne pourrait être prise qu\'après une évaluation
minutieuse du paysage de la certification et des conséquences
négatives possibles, telles que la sortie de ressources précieuses
utilisées par les communautés de l'EOSC et la mise en danger de ces
ressources.**

**Nous recommandons vivement que les entrepôts et les services qui
souhaitent rejoindre l\'EOSC utilisent les critères du cadre de
certification pour vérifier et améliorer leurs pratiques, dans le but
de progresser vers la certification**. **Les entrepôts certifiés
doivent être clairement identifiés comme tels.**

Nous considérons que CoreTrustSeal, qui est un cadre international
piloté par la communauté et utilisé par une large palette de
disciplines, est le bon niveau pour les entrepôts de données de
recherche gérés dans l\'environnement scientifique au regard des
normes DIN 31644 (nestorseal) et ISO 16363:2013.

Les travaux existants sur la certification des services requis pour
mettre en œuvre les principes FAIR devraient être étendus au cours du
prochain programme-cadre et garantir l\'applicabilité dans toutes les
disciplines. Il ne faut pas chercher à définir la certification pour
tous les types de services de l\'écosystème FAIR. Des priorités
devraient être établies sur les services à certifier.

L\'approche de la capacité/maturité proposée dans CoretrustSeal+FAIR
doit être testée de manière approfondie. Tous les cadres de
certification proposés pour d\'autres composants de l\'écosystème FAIR
devront également être mis à l'épreuve de manière approfondie et les
réactions de diverses parties prenantes devront être recueillies.

*Turning FAIR into Reality* indique que \"un soutien concerté est
nécessaire pour aider les entrepôts existants à obtenir la
certification\"[^1]. Les besoins peuvent concerner notamment
l\'élaboration et la mise en œuvre des normes communautaires
nécessaires pour respecter les principes FAIR, ainsi que la
constitution de compétences et d'une main-d\'œuvre nécessaires à la
gestion des données et à la mise en œuvre de FAIR. Le soutien à
l\'auto-évaluation des services selon les critères de certification
est nécessaire pour renforcer l\'écosystème et garantir que nous
pouvons compter sur le Web des données et des services FAIR. La
certification d\'un entrepôt implique des coûts, qui dépendent du
statut de départ de l'entrepôt en matière de certification. Les
parties prenantes, notamment les collectivités publiques en charge des
entrepôts et les bailleurs de fonds, peuvent comprendre les gains de
de l\'auto-évaluation et de la certification par rapport à des normes
de qualité et une démonstration de fiabilité, mais dans certains cas,
les coûts peuvent être prohibitifs en ressources. En l\'absence de
possibilités de financement, les pays, institutions ou disciplines ne
disposant pas des ressources appropriées pourraient ne pas être en
mesure d\'obtenir la certification, et le fossé entre les pays ou
disciplines développés et moins développés se creuserait davantage.

Plus généralement, un soutien devrait être fourni pour certifier les
services mettant en œuvre les principes FAIR une fois que le programme
de certification spécifique est défini.

*Turning FAIR into Reality* indique que \"des mesures doivent être
prises pour garantir que les organisations supervisant les systèmes de
certification sont indépendantes, fiables, durables et
évolutives.\"[^2] L\'évolutivité est nécessaire pour faire face à
l\'augmentation du nombre d'entrepôts cherchant à obtenir une
certification de \"niveau de base\" en raison des incitations
politiques. Un élément d\'évolutivité pourrait consister à développer
des normes communautaires convenues qui pourraient être évaluées par
des systèmes automatiques, étant des composants prédéfinis du
processus de certification. Un soutien peut être nécessaire à un
moment donné pour assurer l\'évolutivité, mais il doit tenir compte de
l\'indépendance nécessaire de l\'organisation et la préserver. Ce
soutien pourrait être fourni pour permettre la certification
d'entrepôts de pays et de disciplines disposant de ressources
financières limitées.

La gouvernance et la mise à jour de ce rapport et de ses
recommandations doivent se faire en partenariat étroit avec les
communautés d\'utilisateurs. Un groupe de travail du forum des parties
prenantes de l\'EOSC devrait être créé pour en assurer le suivi, et
chargé d\'évaluer l\'évolution des activités en cours et du paysage de
la certification, y compris les cadres de certification et
l\'évolution des capacités des entrepôts en lien avec la
certification.

L\'analyse de la situation, des lacunes et des possibilités
d\'extension permet de définir des priorités pour les travaux futurs à
court terme :

**[Priorité 1]{.underline}** : soutenir les efforts actuels pour
aligner les normes de certification et les schémas d\'évaluation avec
FAIR.

**[Priorité 2]** : tester les schémas proposés dans
diverses communautés afin de recueillir des commentaires et de mettre
à jour le cadre proposé en conséquence.

**[Priorité 3]** : fournir un soutien, tant méthodologique
que financier, aux fournisseurs de données et de services pour
progresser vers la certification.

**[Priorité 4]** : suivre l\'évolution de la
certification, évaluer la maturité de ce paysage et prendre les
mesures appropriées si des domaines ou des régions accusent un certain
retard.

**[Priorité 5]** : soutenir l\'établissement de critères
de base et d\'une méthodologie pour certifier d\'autres éléments clés
de l\'écosystème FAIR, notamment, en premier lieu, les services
d'identifiants pérennes (PID) et les référentiels de
vocabulaire/registres de métadonnées, et les tester à grande échelle.

**[Priorité 6]** : soutenir l\'établissement et la tenue
de registres des composants certifiés de l\'écosystème ; si plusieurs
registres sont disponibles pour un composant donné, ils doivent
pouvoir être moissonnés et inclus dans des registres de registres.

**[Priorité 7] :** créer un groupe de travail sous
l\'égide du forum des parties prenantes de l\'EOSC pour assurer la
mise en œuvre et le développement ultérieur des recommandations de ce
rapport.

[^1]: « Concerted support is necessary to assist existing repositories
    in achieving certification ». Source : Action 9.2 page 67 du rapport
    <https://ec.europa.eu/info/sites/default/files/turning_fair_into_reality_1.pdf>

[^2]: « Steps need to be taken to ensure that the organisations
    overseeing certification schemes are independent, trusted,
    sustainable and scalable ». Source: Action 13.4 page 69 du rapport
    <https://ec.europa.eu/info/sites/default/files/turning_fair_into_reality_1.pdf>
