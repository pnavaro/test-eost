# Mise en œuvre d'un EOSC inclusif

15-18 juin 2021

#EOSCSymposium2021

eoscsecretariat.eu

Comité du programme du symposium de l'EOSC 2021

Le Comité du programme du symposium de l'EOSC 2021 est composé de représentants faisant partie des communautés et du paysage de l'EOSC, ainsi que de représentants de l'association EOSC

Juan Bicarregui

RDA, UKRI & Bureau exécutif de l'EOSC

Donatella Castelli

CNR-ISTI & EOSCsecretariat.eu

Suzanne Dumouchel

CNRS & Bureau de l'association EOSC

Nicholas Ferguson

Trust-IT Services & EOSCsecretariat.eu

Bob Jones

CERN & Bureau de l'association EOSC

Sarah Jones

GÉANT & Bureau de l'association EOSC

Karel Luyben

CESAER & Bureau exécutif de l'EOSC

Anastas Mishev

UKIM & NI4OS-Europe

Paulo Quaresma

FCT & Délégué du comité directeur pour le Portugal de l'EOSC

Table des matières

Introduction

Mardi 15 juin 2021 - Écosystème de l'EOSC

Mercredi 16 juin 2021 - Gouvernance de l'EOSC et association EOSC

Jeudi 17 juin 2021 - Priorités de mise en œuvre

Vendredi 18 juin 2021 - Priorités de mise en œuvre

Clause de non-responsabilité

Les informations et les points de vue exposés dans ce rapport sont ceux des auteurs et ne reflètent pas nécessairement l'opinion officielle de la Commission européenne, qui ne peut donc pas être tenue pour responsable de l'usage qui pourrait être fait des informations qu'il contient.

Août 2021

Remerciements

Nous tenons à remercier la Commission européenne, la Présidence portugaise du Conseil de l'Union européenne, l'association EOSC et la communauté de l'EOSC. Nous tenons également à remercier les rapporteurs du projet EOSCsecretariat.eu qui ont fourni les résumés des sessions inclus dans ce document :

Auteurs

Veronica Bertacchini, Technopolis

Federico Drago, Trust-IT Services

Katharina Flicker, Université technique de Vienne

Netsanet Gebreyesus, KIT

Annabel Grant, GÉANT

Bob Jones, CERN

Iiris Liinamaa, CSC-IT Centre de technologie de l'information pour la science

Anu Märkälä, CSC-IT Centre de technologie de l'information pour la science

Christos Marinos-Kouris, ARC

Bert Meerman, Go Fair Foundation

Bernd Saurugger, Université technique de Vienne

Zachary Smith, Trust-IT Services

Programme

Suivez le lien ci-dessous pour accéder au programme détaillé, aux enregistrements des sessions et aux présentations.

www.eoscsecretariat.eu/eosc-symposium-2021-programme

Ce rapport a été produit par le projet EOSCsecretariat.eu, qui a reçu un financement du programme Horizon de l'Union européenne pour l'appel à projet H2020-INFRAEOSC-05-2018-2019, accord de subvention n° 831644.

De Bruxelles à Budapest, nous tenons à vous remercier, vous, les parties prenantes de l'EOSC, qui avez contribué aux événements du symposium de l'EOSC organisés par EOSCsecretariat.eu. Ensemble, nous allons mettre en œuvre un European Open Science Cloud inclusif.

Nous avons hâte de voir ce que l'avenir nous réserve, du prochain symposium et de bientôt vous retrouver.

Introduction

Le symposium de l'EOSC 2021 a apporté une opportunité d'engagement clé pour la communauté de l'EOSC après son entrée très attendue en phase de mise en œuvre, en 2021. Organisé en ligne pour un peu moins de 1 000 parties prenantes de l'EOSC provenant de plus de 63 pays, il s'agissait non seulement du plus grand symposium de l'EOSC à ce jour, mais aussi d'une occasion importante de convergence et d'alignement sur les principes et les priorités de l'EOSC.

L'association EOSC jouera un rôle important dans cette phase. Avec déjà plus de 210 organisations membres et observatrices dans toute l'Europe, l'association représente une voix unique pour la promotion et la représentation de la communauté plus large des parties prenantes de l'EOSC en Europe, en promouvant l'alignement de la politique et des priorités de recherche de l'UE.

L'association fera évoluer de façon continue l'Agenda stratégique pour la recherche et l'innovation (SRIA), qui influencera les futures activités de l'EOSC aux niveaux institutionnel, national et européen (y compris les programmes de travail liés à l'EOSC d'Horizon Europe). Ce document évolutif s'adaptera aux modifications de l'écosystème de l'EOSC et aux besoins de ses parties prenantes. L'association met en place une série de groupes consultatifs (AG) et de task forces (TF) pour travailler avec les membres de la communauté de l'EOSC autour de domaines prioritaires, à savoir : la **mise en œuvre** de l'EOSC, ses **problématiques techniques**, **la qualité des données et des métadonnées**, les **carrières et formations des chercheurs**, et la **pérennisation** de l'EOSC. Le symposium a été la première occasion pour l'association de présenter les projets de chartes des task forces. De plus, l'un des principaux objectifs de l'événement était de lui permettre de comprendre quels travaux sur les thèmes des AG et des TF ont été réalisés, en cours ou prévus. Un appel à contributions a été lancé tout au long du mois de mai 2021, totalisant 137 candidatures. Grâce à des présentations, des exposés éclairs et des panels, plus de 70 membres de la communauté ont pu soumettre les principales conclusions et recommandations que les AG et les TF doivent prendre en considération dans leurs travaux.

Le symposium a eu lieu quelques semaines avant que l'European Open Science Cloud (EOSC) reçoive le statut spécial de partenariat européen coprogrammé. Ce nouveau statut lui permettra de bénéficier d'un financement européen de près de 500 millions d'euros et d'une contribution en nature de la part des partenaires de 500 millions d'euros supplémentaires. Le premier jour de l'événement a été consacré à l'écosystème de l'EOSC, avec des discours de la Commission européenne, de la présidence portugaise de l'UE et de l'association EOSC. Cette journée s'est poursuivie par une présentation des futurs appels à propositions de l'EOSC dans le cadre du programme Horizon Europe.

Le deuxième jour a été marqué par le lancement du thème du reste du symposium, en se penchant sur les véritables priorités de la mise en œuvre de l'EOSC. La structure des AG et des TF a été présentée, et les informations pour faire partie des task forces ont été fournies. Les groupes consultatifs et les task forces sont l'essence même du symposium, avec des sessions dédiées au cours desquelles les projets de charte ont été partagés et d'autres contributions de la communauté des parties prenantes ont été présentées.

Le fil conducteur de la première de ces sessions était les carrières et formations des chercheurs, en mettant en avant le rôle clé que l'EOSC peut jouer dans le changement culturel nécessaire pour que la science ouverte et les données FAIR soient mieux connues, jusqu'à devenir la norme au sein de la communauté scientifique, et pour créer l'infrastructure nécessaire à la modernisation du système de récompense universitaire.

La session de l'AG à propos de la pérennisation de l'EOSC s'est concentrée sur les deux principaux sujets de la TF : la viabilité financière et la conservation durable à long terme des données et autres objets numériques utiles pour la recherche. La session a mis en évidence l'importance de la pérennisation à long terme des services, allant au-delà du financement des projets. Le rôle important des fournisseurs commerciaux et de leurs services qui devraient être gratuits au point d'utilisation a également été le sujet de cette session.

La session la plus suivie de cet évènement a été celle portant sur les problématiques techniques auxquelles l'EOSC doit faire face, avec un peu moins de 40 candidats à l'appel à contributions. Elle a été divisée en plusieurs sessions couvrant les trois TF : l'infrastructure pour des logiciels de recherche de qualité, l'architecture de l'infrastructure d'authentification et d'autorisation (AAI) et l'interopérabilité technique des données et des services. Les contributions et les activités d'un large éventail d'initiatives et de projets financés par la Commission européenne ont également été présentées.

De même, la session sur la mise en œuvre de l'EOSC a accueilli un peu moins de 40 candidats et des sessions en petits groupes ont eu lieu. L'AG sur la mise en œuvre de l'EOSC se concentre sur la manière de déployer les recommandations de l'EOSC et de tester leur applicabilité avec les communautés de recherche et les fournisseurs de services. Ce groupe cherche également à promouvoir une adoption plus large de l'EOSC, en particulier au sein de la communauté de la recherche.

Enfin, lors de la session sur la qualité des données et des métadonnées, les sujets traités étaient l'interopérabilité sémantique pour garantir la découverte et la réutilisation des données, ainsi que les aspects de la qualité des données et des indicateurs pour promouvoir des ressources de grande qualité dans l'EOSC.

Afin de faciliter davantage l'échange entre l'association et la communauté, des EOSC-Clinics ont été organisés pendant les pauses-déjeuner de l'événement. Ces sessions ouvertes de questions-réponses étaient animées par des membres du conseil d'administration de l'association EOSC qui répondaient aux questions du public, dans une ambiance agréable.

Des sessions supplémentaires ont été mises en place pendant le symposium sur d'autres sujets clés de la phase de mise en œuvre. La première de ces sessions avait pour problématique les défis et les meilleures pratiques concernant l'engagement et les mécanismes de coordination de l'EOSC au niveau national.

La seconde avait pour sujets de réflexion les politiques de science ouverte, et l'identification et la réduction des obstacles politiques. La session a mis en évidence l'incitation des chercheurs et des futures parties prenantes/communautés de l'EOSC à adopter des politiques nationales, européennes et institutionnelles pour aider à l'harmonisation des différentes initiatives et ainsi implémenter la science ouverte dans le futur.

La troisième session portait sur l'importance des aspects concernant l'utilisateur et la co-création de l'EOSC pour favoriser un engagement plus fort. Durant la session, l'intégration des infrastructures de recherche et de leurs communautés thématiques ont été suggérées pour répondre aux besoins actuels de la communauté de la recherche, de leur donner la priorité et d'apporter une valeur ajoutée à leurs utilisateurs finaux.

Enfin, les missions du projet EOSCsecretariat.eu ont été présentées pour souligner son rôle clé : d'une part, en soutenant la gouvernance de l'EOSC dans la mise en place du Bureau exécutif de l'EOSC, de ses six groupes de travail, et en soutenant la production de 20 rapports qui ont conduit à la création de l'association et d'autre part, en instituant le symposium de l'EOSC comme un événement clé pour les parties prenantes. Il sera soutenu par le projet nouvellement financé EOSC Future. Ce dernier a fait l'objet d'un certain nombre de sessions tout au long du symposium, étant donné qu'un grand nombre des activités liées à la création du socle minimal de l'EOSC décrit dans le SRIA seront entreprises par le projet. EOSC Future permettra de mettre en œuvre un EOSC-Core opérationnel, un système de systèmes, qui agira comme une « couche de colle » combinant les ressources à travers les infrastructures, et il alimentera la plateforme EOSC-Exchange.

Mardi,

15 juin 2021

Écosystème de l'EOSC

9:00-10:30 Discours d'ouverture

Présidente de session

Marialuisa Lavitrano

Université Milano-Bicocca & directrice, association EOSC

Introduction

Le Symposium de l'EOSC a été inauguré par Marialuisa Lavitrano (Université Milano-Bicocca et directrice, association EOSC), membre du bureau de l'association EOSC, qui a présenté la mise en place de l'association et son rôle important dans la phase de mise en œuvre de l'EOSC.

Le point de vue de la Commission européenne a été présenté par Kostas Glinos (Chef de l'unité Science ouverte, Commission européenne) et Liina Munari (Chef d'unité adjoint, Science ouverte et modélisation numérique, DG CONNECT, Commission européenne). La Commission européenne soutient une politique de science ouverte qui renforce la confiance dans un environnement où le processus de recherche est plus ouvert et solide grâce au numérique, non seulement pour les chercheurs, mais aussi pour le grand public. L'EOSC fournit l'infrastructure permettant d'atteindre cet objectif et cherche à transformer le paysage scientifique européen.

Ute Gunsenheimer (Secrétaire Générale désignée, association EOSC) a donné un aperçu des principaux objectifs de l'association et Paulo Quaresma (Conseil d'administration de la FCT et Délégué du comité directeur de l'EOSC) a souligné le rôle de l'association en ce qui concerne l'établissement du partenariat de l'Union européenne avec la Commission européenne et de la création de task forces qui se chargeront de l'agenda politique. Ce partenariat ouvrira la voie à l'approfondissement des pratiques de la science ouverte dans le nouvel environnement européen de la recherche et à la contribution au Digital Agenda for Europe. L'EOSC sera considéré comme la fondation de recherche de tous les Data Spaces sectoriels définis dans la stratégie européenne pour les données.

João Moreira (FCT/FCCN & association EOSC) a présenté l'EOSC du point de vue de la présidence portugaise de l'UE en soulignant le rôle que l'EOSC peut jouer dans le développement de services et de produits innovants qui peuvent accroître la collaboration, faciliter une livraison et une réutilisation des résultats plus rapide, et favoriser le développement de services à valeur ajoutée.

Visionner la session complète

Principaux points à retenir

\- Le symposium de l'EOSC intervient à un moment crucial du point de vue stratégique, où la phase de mise en œuvre commence et que l'association EOSC et ses groupes consultatifs et task forces prennent forme.

\- L'association EOSC a été créée en juillet 2020 en tant qu'association internationale à but non lucratif (AISBL) en Belgique. Avec déjà plus de 210 organisations membres et observatrices dans toute l'Europe, l'association représente une voix unique pour la promotion et la représentation de la communauté plus large des parties prenantes de l'EOSC en Europe, en promouvant l'alignement de la politique et des priorités de recherche de l'UE. L'un des rôles essentiels de l'association est de faire évoluer de façon continue l'agenda stratégique pour la recherche et l'innovation (SRIA) de l'EOSC qui aura un impact sur ses futures activités aux niveaux institutionnel, national et européen (y compris les programmes de travail d'Horizon Europe liés à l'EOSC). Pour ce faire, l'association met en place une série de groupes consultatifs (AG) avec des task forces (TF) pour s'engager avec la communauté de l'EOSC autour de domaines prioritaires, à savoir : la **mise en œuvre** de l'EOSC, ses **problématiques techniques**, **la qualité des données et des métadonnées**, les **carrières et formations des chercheurs**, et la **pérennisation** de l'EOSC.

\- L'association jouera un rôle clé dans le nouveau partenariat avec l'UE, qui marque le tournant d'un nouveau modèle de gouvernance pour l'EOSC, plaçant les parties prenantes à travers l'Europe dans une position de force. Le partenariat représente un nouveau modèle de gouvernance alors que la première phase de mise en œuvre de l'EOSC commence. Ce partenariat est composé de la Commission européenne et de l'association EOSC nouvellement formée. Le comité de partenariat invite des représentants des MS et des AC faisant partie du groupe d'experts du « Comité de pilotage de l'EOSC ».

\- En s'appuyant sur l'agenda stratégique pour la recherche et l'innovation (SRIA) v1.0 de l'EOSC, le partenariat comprend plusieurs objectifs clés pour garantir la définition de normes et le développement de services et d'outils. Il permettra de faire évoluer les communautés scientifiques et les infrastructures de recherche vers un accès transparent et une réutilisation fiable des données, méthodes, logiciels et publications, et ce à l'intérieur comme au-delà des disciplines et des frontières.

\- L'ambition de l'EOSC est de développer un écosystème inclusif, ouvert et dynamique où les pratiques et les compétences en matière de science ouverte sont récompensées et enseignées, un écosystème où la gestion des données FAIR deviendrait la norme, avec des conditions d'exploitation qui préservent la confiance et l'intérêt public. En soutenant la politique de l'UE en matière de science ouverte et la stratégie européenne en matière de données, l'EOSC entend aider les scientifiques européens à mettre à profit tous les avantages de la science basée sur les données et positionner l'Europe au rang de leader mondial dans la gestion des données de recherche.

Prochaines étapes

Un protocole d'accord pour le partenariat de l'EOSC dans le programme Horizon Europe a été signé le 23 juin 2021. Les premières réunions du comité directeur auront également lieu en juin 2021.

\- Les États membres et les pays associés, ainsi que leurs organismes de financement nationaux, jouent un rôle clé dans le partenariat, car ils financeront directement les développements des infrastructures de recherche qui doivent être conformes aux normes de l'EOSC et facilement intégrées à celui-ci. Ils développeront également des politiques qui stimuleront et soutiendront les organisations existantes dans leurs pays afin qu'elles se conforment autant que possible à l'EOSC et qu'elles partagent leurs ressources de recherche par le biais de l'EOSC.

\- L'une des missions principales de l'association est la réalisation continue du SRIA (actuellement v1.0). Il s'agit d'un document évolutif qui s'adaptera aux changements de l'écosystème de l'EOSC et aux besoins de ses parties prenantes dans des versions futures, qui sera mis à jour par l'association EOSC avec la contribution de la Commission européenne. Le calendrier des itérations futures sera défini par l'association.

\- Les task forces (TF) sont un élément essentiel de cette tâche et le symposium est un événement clé pour celles-ci. Il présentera tout d'abord les projets de charte des 13 TF (qui étaient en cours de finalisation au moment de l'événement) et nous informe des travaux en cours ou prévus de la communauté de l'EOSC. Un appel à membres pour les TF se déroulera tout au long du mois de juillet 2021, et leur mise en place se poursuivra jusqu'en septembre.

Mardi 15 juin 2021

Ecosystème de l'EOSC

11:00-12:30 Futurs appels de l'EOSC dans le programme Horizon Europe

Présidents de session

Liina Munari

DG CONNECT, Commission européenne

Michel Schouppe

DG RTD, Commission européenne

Introduction

La session *Futurs appels de l'EOSC dans le programme Horizon Europe* donne un aperçu du travail qui a été mené ces dernières années, en particulier celui du SRIA avec ses champs d'action identifiés. L'objectif principal du nouveau programme Horizon Europe a été présenté lors de cette session. En outre, les futurs appels INFRAEOSC, qui sont établis selon les enjeux identifiés par le SRIA, les objectifs, les activités et les éventuelles synergies avec les projets existants sont présentés.

Principaux points à retenir

Le résumé suivant\[1\] comprend des informations présentées lors du symposium et des journées d'information sur les infrastructures de recherche.

\- La destination INFRAEOSC de la partie « Infrastructures de recherche » du programme de travail d'Horizon Europe pour 2021-2022 a été élaborée suite aux contributions reçues dans le cadre du partenariat européen co-programmé pour l'European Open Science Cloud par le biais de son agenda stratégique pour la recherche et l'innovation. Celui-ci a fixé les priorités d'importance majeure qui doivent être gérées par le biais du financement européen.

\- L'agenda stratégique pour la recherche et l'innovation (SRIA) de l'EOSC contient une feuille de route détaillée pour les deux prochaines années, comprenant à la fois les activités prioritaires et les résultats escomptés. Le SRIA a été largement consulté par les parties prenantes concernées et propose une répartition des trois objectifs généraux à atteindre par le partenariat de l'EOSC (concernant les personnes, les données et l'infrastructure) en une liste de quatorze domaines d'action concrets, eux-mêmes ventilés en sept domaines liés aux difficultés de la mise en œuvre (par exemple, les identifiants, les métadonnées, les systèmes d'indicateurs FAIR et leur certification, etc.) et sept domaines liés aux conditions limites (compétences et formation, communication, modèles de financement, récompenses et reconnaissance, etc.)

\- La destination INFRAEOSC prévoit un budget estimé à près de 90 millions d'euros pour financer dix projets dans le cadre du WP 2021-2022, respectivement six projets dans l'appel à propositions de 2021 et quatre dans celui de 2022. Des investissements supplémentaires de l'UE dans le cadre de la mise en œuvre de l'EOSC seront également réalisés en 2021-2022 pour un marché public soutenant l'EOSC-Core (environ 35 millions d'euros) et une subvention sera attribuée pour le partage des données ouvertes et FAIR en soutien à la préparation européenne pour la Covid-19 et d'autres maladies infectieuses (environ 12 millions d'euros) dans le cadre de l'appel dénommé : HORIZON-INFRA-2021-EMERGENCY-01.

\- Les sujets proposés dans le cadre de la destination INFRAEOSC sont liés aux quatorze domaines d'action présentés dans le SRIA de l'EOSC.

\- Les projets financés dans le cadre de la destination INFRAEOSC favoriseront la mise en œuvre et l'implémentation de l'écosystème de l'EOSC afin de concrétiser la vision d'un Web de données et de services FAIR pour la science.

\- Grâce aux projets financés, les objectifs généraux définis dans le SRIA pourront avancer :

\- Les projets contribueront à l'intégration de la science ouverte comme nouvelle norme ;

\- Les projets contribueront à une fédération accrue des infrastructures de recherche ;

\- Les projets contribueront à la promotion des principes FAIR et des données ouvertes, conformément à la logique selon laquelle les données doivent être aussi ouvertes que possible et aussi fermées que nécessaire.

Prochaines étapes

Retrouvez tous les détails sur les appels d'Horizon Europe liés à l'EOSC ici.

Mardi 15 juin 2021

Ecosystème de l'EOSC

13:45-15:15 les mécanismes d'engagement de l'EOSC au niveau national

Présidente de la session

Sara Garavelli

CSC & EOSCsecretariat.eu

Introduction

L'engagement et l'adhésion des utilisateurs sont perçus comme des facteurs essentiels dans le succès de l'EOSC. Cette session a exploré la façon dont les pays européens contribuent à cette activité en faisant ressortir les meilleures pratiques et les enjeux actuels. Les premiers résultats d'une étude menée par l'EOSCsecretariat.eu sur les mécanismes d'engagement et de coordination au niveau national de l'EOSC ont été présentés, ainsi que les initiatives nationales pour la science ouverte telles que NI4OS. La session s'est terminée par une discussion de groupe sur les initiatives et les mécanismes nationaux pour coordonner les activités de l'EOSC dans un certain nombre de pays.

Visionner la session complète

Principaux points à retenir

\- Les principaux objectifs de l'étude EOSCsecretariat.eu sur les mécanismes d'engagement et de coordination de l'EOSC au niveau national comprennent la cartographie de ses mécanismes de coordination et d'engagement existants et émergents au niveau national, la description des meilleures pratiques, des principaux défis et les avantages de ces mécanismes nationaux, le positionnement des mécanismes de coordination et d'engagement nationaux dans l'écosystème de l'EOSC, et les recommandations sur la façon d'améliorer son engagement aux niveaux national et européen. Ce rapport remplit trois rôles clés :

\- Premièrement, il sert de contribution au travail futur de la gouvernance de l'EOSC.

\- Deuxièmement, il peut être utilisé comme un modèle pour tous les pays de l'UE désireux de mettre en place les mécanismes de coordination et d'engagement nationaux de l'EOSC.

\- Troisièmement, il s'agit d'un document de référence pour toutes les parties prenantes de l'EOSC afin de mieux comprendre ses acteurs nationaux.

\- Actuellement, 11 pays ont déjà contribué à l'étude, tandis que 20 autres qui ont l'intention de mettre en place des mécanismes de coordination des activités de l'EOSC sont observés de près. Les premiers résultats montrent que ces mécanismes sont susceptibles de différer fortement d'un pays à l'autre car leurs besoins varient.

\- Les facteurs de réussite au niveau national comprennent, par exemple, un engagement fort à tous les niveaux (y compris au niveau politique), l'implication des acteurs clés (en particulier les universités), peu de restrictions d'entrée et de bons canaux de communication. À un niveau plus large, ces facteurs comprennent une proposition de valeur claire de l'EOSC, un plan de pérennisation, la qualité des données et des services et leur interopérabilité, ainsi que le développement de compétences et d'aptitudes pour garantir des processus qui respectent les principes FAIR et pour soutenir les actions liées à l'EOSC.

\- Le projet National Initiatives for Open Science (NI4OS) est un des contributeurs essentiels de l'EOSC, qui assure dans le même temps l'inclusion au niveau européen de la Science Ouverte (OS). Actuellement, 15 États membres et pays associés y participent. Dans ce contexte, la mise en place d'initiatives nationales pour la science ouverte fait face à plusieurs obstacles. Tout d'abord, 14 langues différentes sont parlées parmi ces 15 membres et les pays diffèrent grandement en termes d'avancées des politiques et des initiatives de la science ouverte, ainsi que des infrastructures et des politiques d'éducation. De plus, l'instabilité politique affecte à la fois l'introduction et la mise en œuvre des politiques de science ouverte. Il est donc nécessaire de disposer d'une solution modulaire qui puisse être adaptée aux besoins spécifiques de chaque État.

\- Les discussions de groupe ont permis à 8 pays (Autriche, République tchèque, Italie, Finlande, France, Allemagne, Espagne et Suède) de partager leurs expériences et leurs approches de la mise en place de mécanismes nationaux destinés à coordonner les activités de l'EOSC. Même si leurs approches diffèrent significativement en termes de degré d'implication des gouvernements et des ministères par exemple, ou de choix d'une approche ascendante ou descendante, un thème commun a été identifié : la nécessité de faire entendre la voix de toutes les parties prenantes.

Prochaines étapes

-   L'étude de l'EOSCsecretariat sur les mécanismes d'engagement et de coordination de l'EOSC au niveau national fournira des informations utiles pour le travail futur de la gouvernance de l'EOSC. Elle peut servir de modèle aux pays qui souhaitent mettre en place des mécanismes nationaux pour soutenir les activités de l'EOSC et donne un aperçu du statu quo des acteurs nationaux. En outre, elle énumère plusieurs facteurs indispensables pour la mise en place d'initiatives nationales.

-   L'NI4OS a l'expérience nécessaire pour mettre en place les politiques et les initiatives nationales en matière de sécurité opérationnelle. Elle connaît bien les défis liés à cette activité, notamment dans les pays participants, et sera un partenaire précieux pour renforcer les pratiques de science ouverte et en faire la norme.

-   Au cours des discussions de groupe, les intervenants et le public ont identifié de nombreux aspects essentiels à la réussite de la mise en œuvre des initiatives nationales. Deux éléments majeurs sont toutefois sortis du lot : la nécessité de soutenir l'échange de connaissances et d'apprendre des expériences d'autrui, et la nécessité de trouver des moyens d'impliquer toutes les parties prenantes et de faire entendre leurs voix.

Mardi 15 juin 2021

L'Ecosystème de l'EOSC

11h-12h30 Table ronde sur les politiques. Une discussion active sur la mise en œuvre des politiques de science ouverte dans des projets régionaux

Président de session

Jos van Wezel

KIT & EOSCsecretariat.eu

Introduction

Une multitude de projets liés la sphère de l'EOSC suivent un processus de consultation menant à l'échange d'idées et de recommandations sur la manière de faciliter la fédération dans le cadre de l'EOSC. Dans ce contexte, il est important de mettre l'accent sur l'identification et la réduction des barrières politiques, ainsi que sur l'incitation des chercheurs et des futures parties prenantes/communautés de l'EOSC à adopter des politiques nationales, européennes et institutionnelles qui participeront à l'harmonisation des différentes initiatives menant à l'ouverture de la science.

Visionner la session complète

Principaux points à retenir

\- Une croissance organique importante peut être observée dans les politiques à travers toute l'Europe car l'EOSC développe des approches communes dans le développement des politiques qui devraient être appliquées.

\- Une communication claire est importante pour définir les principes FAIR et informer les chercheurs des moyens possibles pour qu'ils harmonisent leur travail en termes de gestion des données.

\- La science ouverte est actuellement freinée par des obstacles juridiques. Simplifier les aspects juridiques et définir des exceptions devraient être une priorité. En outre, l'UE devrait mettre l'accent sur la compatibilité des utilisateurs et l'interopérabilité des données de manière horizontale lorsqu'elle façonne le paysage juridique.

\- Le cadre juridique devient plus solide dans son ensemble. Cependant, la mise en œuvre de ce cadre devient de plus en plus difficile pour les chercheurs.

\- Les politiques adoptées par les grandes institutions sont transposées au niveau national. Les petites organisations sont généralement plus souples dans l'adoption des politiques qui correspondent à leurs objectifs. Il est important d'aborder une approche coordonnée visant à inclure une multitude d'acteurs dans des forums communs pour assurer une mise en œuvre horizontale et une harmonisation de ces politiques.

Prochaines étapes

\- L'effort pour développer et fournir des outils ne doit pas être limité, mais étendu pour refléter également l'approche ascendante de l'harmonisation et de la normalisation, qui devrait être prise en considération dans les lois de l'UE.

\- Des éclaircissements devraient être apportés au sujet de la couverture des futurs aspects politiques aux niveaux national, européen et institutionnel. Par conséquent, le type de soutien et d'actions à entreprendre devrait être identifié afin d'aider les communautés de recherche à adopter ces politiques et s'y conformer.

\- Les institutions, les bailleurs de fonds et les décideurs politiques devraient entamer des discussions autour d'une table sur l'élaboration des politiques, tout en essayant de trouver une approche commune pour suivre les progrès, partager les meilleures pratiques et incitations, ainsi que les outils de mise en œuvre des politiques qui ont été développés.

Mercredi 16 juin 2021

La gouvernance de l'EOSC et l'association EOSC

9h-10h45 Les priorités durant la phase de mise en œuvre de l'EOSC -- Sujets et chartes des groupes consultatifs et des task forces de l'association EOSC

Président de la session

Karel Luyben

CESAER et Président, association EOSC

Introduction

Durant cette session, les groupes consultatifs (AG) de l'EOSC, leurs task forces (TF) et leurs chartes ont été présentés. Ils constitueront un élément important de la phase de mise en œuvre de l'EOSC. En outre, les prochaines étapes liées à la composition des AG, telles que le processus de candidature, les critères de sélection et le choix des présidents, ont été décrites. La session s'est terminée par une séance de questions-réponses.

Principaux points à retenir

\- Les groupes consultatifs sont au nombre de cinq. Ils se nomment : *mise en œuvre de l'EOSC*, *problématiques techniques de l'EOSC*, *qualité des données et des métadonnées*, *carrières et formations des chercheurs* et *pérennisation de l'EOSC*. Chacun d'eux comprend différentes task forces.

\- Les task forces *Contrôle de conformité des règles de participation*, *Politique et mise en œuvre des identifiants pérennes (PID), Intégration et adoption des chercheurs* sont rattachées au groupe consultatif « *Mise en œuvre de l'EOSC »*. Les TF *Interopérabilité technique des données et services*, *Infrastructure pour des logiciels de recherche de qualité* et *Architecture AAI* sont assignés à l'AG « *Problématiques techniques de l'EOSC »*. L'AG « *qualité des données et des métadonnées »* comprend les TF *Interopérabilité sémantique * et *Indicateurs et* *qualité des données FAIR*. Trois autres TF -- *Programmes de formations et carrières en administration des données, Développement des compétences nationales pour une intégration dans l'EOSC, Carrières de la recherche, reconnaissance et valorisation* -- appartiennent à l'AG Carrières et formations des chercheurs, tandis que les deux dernières TF *Définition de modèles de financement pour l'EOSC*  et *Conservation à long terme des données* appartiennent à l'AG « *Pérennisation de l'EOSC* » (les réponses à la FAQ sur les TF se trouvent ici).

\- Les TF sont mises en place en deux étapes. La première, qui a duré jusqu'en juin 2021, est la rédaction des projets de chartes définissant le champ d'application et les activités de chaque charte. La seconde consiste à recruter des membres de la communauté pour mettre en œuvre les activités proposées dans les chartes (pour plus d'informations sur les chartes, cliquez ici).

\- Le système d'adhésion aux TF reste à définir. Des critères de sélection seront établis si nécessaire. Chaque charte donne un aperçu des compétences et aptitudes nécessaires dans le contexte de leurs objectifs et activités spécifiques. Un appel à adhésion sera ouvert de la mi-juin à la mi-juillet.

\- Dans certains cas, il a été suggéré, pour des raisons pragmatiques, de limiter le nombre de membres des TF. Cette idée a cependant été mal accueillie car elle risque de porter préjudice à l'inclusivité, en soutenant plutôt des clubs fermés.

Prochaines étapes

\- Le système d'adhésion doit être défini pour avancer dans la mise en œuvre de l'EOSC. En outre, les membres des TF doivent être recrutés afin que ces task forces puissent commencer leurs activités.

\- La limitation du nombre de membres des TF ayant suscité des inquiétudes par rapport au degré d'inclusivité, de transparence et d'ouverture des travaux futurs, il peut être raisonnable de (a) développer des contre-stratégies afin d'éviter de créer des groupes restrictifs, et (b) expliciter le fait que ce nombre a été limité pour des raisons pragmatiques. L'intention est (comme toujours) d'être aussi ouvert, transparent et inclusif que possible.

\- Parmi les exemples de contre-stratégies suggérées, citons la distinction entre les membres votants/actifs et les personnes extérieures contribuant aux activités, la mise en place de comités de révision composés d'experts autres que les membres des TF et l'envoi régulier de mises à jour. Ces dernières devraient être « digestes » et permettre aux membres de participer ainsi que partager des descriptions claires et des liens utiles. Dans ce contexte, les TF pourraient rédiger des résumés trimestriels destinés à être diffusés et trouver des mécanismes pour synchroniser/harmoniser tous les TF et AG afin d'obtenir une vue d'ensemble.

Mercredi 16 juin 2021

La gouvernance de l'EOSC et l'association EOSC

11h15-13h Priorités, projets et partenariats -- identifier et résoudre les difficultés pour la phase de mise en œuvre de l'EOSC

Présidente de la session

Sarah Jones

GÉANT et directrice, association EOSC

Introduction

Cette session interactive était axée sur les différents défis auxquels les projets mettant en œuvre l'EOSC sont confrontés. Tout d'abord, Sarah a partagé son point de vue sur les raisons pour lesquelles la mise en place de l'EOSC est difficile dans la réalité. Elle a souligné son immense complexité et nous a rappelé que nous sommes toujours dans sa phase de construction. L'association EOSC doit collaborer avec les fournisseurs de services et la communauté de la recherche pour que l'EOSC-Core fonctionne sans problème comme un service public, afin que les utilisateurs puissent se concentrer sur la science. Les défis et les actions clés nécessaires pour les relever ont été l'objet d'une discussion de groupe composé d'Owen Appleton (EGI Foundation), Ingrid Dillo (DANS), Rudolf Dimper (The European Synchrotron (ESRF)), Shalini Kurapati (Clearbox AI, Politecnico di Torino), Hilary Hanahoe, (RDA). Le public a participé à cette discussion par le biais de sondages en direct. Dans l'ensemble, la plupart des défis est plus liée aux aspects sociaux et politiques qu'à l'aspect technique. La sensibilisation, l'engagement des chercheurs et la garantie de l'inclusion dans un contexte multinational où de nombreuses langues sont parlées font partie de ces défis. Du point de vue technique, les défis les plus urgents sont l'interopérabilité, les normes et la création d'un système EOSC transparent et facile à utiliser. Les intervenants et le public ont indiqué que les actions les plus importantes à mettre en œuvre étaient l'engagement des chercheurs, l'étude des besoins des utilisateurs, l'adaptation des communications aux différents groupes et l'adoption des données FAIR comme norme européenne.

Principaux points à retenir

\- L'EOSC, en tant qu'utilitaire de base, doit être assez fluide pour que l'utilisateur ne se rende pas compte qu'il l'utilise, afin qu'il puisse plutôt se concentrer sur l'écosystème construit au-dessus et sur ce que ce dernier a à lui offrir.

\- C'est plus facile à dire qu'à faire, de plus la collaboration entre l'association EOSC, les fournisseurs de services et les communautés de recherche est cruciale pour rendre ceci possible.

\- Les principaux défis auxquels l'EOSC est confrontée sont les suivants : la sensibilisation et l'engagement des utilisateurs (en particulier des chercheurs), la complexité croissante de l'EOSC, la variété des langues et la garantie de l'inclusion, les paiements du côté des fournisseurs et le maintien de l'intérêt et de la confiance d'un public plus large tout en relevant ces défis. Du côté technique, les défis consistent à assurer une gestion efficace des identités, à créer un moteur de recherche inspiré de Google pour que tout le monde puisse trouver les données et les normes, et que le système soit interopérable.

\- Les principales préoccupations sur lesquelles l'association EOSC doit se concentrer sont : la sensibilisation, l'engagement des chercheurs, l'investissement dans davantage de ressources pour l'étude des besoins des utilisateurs, une communication adaptée pour chaque groupe, la gestion des connaissances (c'est-à-dire qui fait quoi, où trouver quoi, etc.), faire des données FAIR une norme en Europe, résoudre les problèmes financiers en ce qui concerne les fournisseurs, et trouver une façon de lier services et données (EOSC-Exchange n'est pas aussi clair qu'EOSC-Core).

Prochaines étapes

-Il vaut mieux mettre en œuvre l'EOSC de manière à le rendre itératif par nature, en réponse aux commentaires des utilisateurs, en incluant de nombreux tests, plutôt que de chercher à le rendre entièrement fonctionnel dès le départ.

-La collaboration entre l'association EOSC, les fournisseurs de services et les communautés de la recherche est essentielle.

-L'association EOSC devrait programmer un forum efficace des parties prenantes afin de représenter tous les intérêts, y compris ceux de la Commission européenne, des États membres, des financeurs, des organismes de recherche, de l'industrie, etc. et en tirer des bénéfices.

-Si les données ne respectent pas les principes FAIR, l'EOSC ne peut pas voir le jour.

Mercredi, 16 juin 2021

Gouvernance de l'EOSC et association EOSC

13:00-14:00 Déjeuner et EOSC-Clinic : Session de questions ouvertes

Présidents de session

Karel Luyben

CESAER & Président, Association EOSC

Sarah Jones

GÉANT & Directrice, Association EOSC

Klaus Tochtermann

ZBW & Directeur, Association EOSC

Introduction

L'EOSC-Clinic a été une occasion précieuse pour le public de poser des questions au Conseil d'administration pendant la pause-déjeuner et de discuter des développements de l'EOSC. Les discussions ont porté sur l'alignement de l'EOSC et les exigences en matière de formation, ainsi que sur la collaboration au niveau mondial et avec GAIA-X.

Principaux points à retenir

\- L'infrastructure EOSC collabore déjà de manière significative avec les universités. La nécessité de comprendre ce qui la différencie des autres infrastructures qui ont participé à la popularité de l'EOSC a été soulignée au cours des discussions. Consulter d'autres entités juridiques pour voir si elles sont intéressées par une collaboration avec l'EOSC fait également partie des préoccupations.

\- La mise en œuvre de l'EOSC est basée sur l'établissement de la confiance au sein de la communauté.

\- La valeur d'éventuelles collaborations au niveau mondial et avec GAIA-X a été relevée au cours de la discussion.

\- Il a été mentionné que « l'EOSC n'est pas une maison » en construction mais devrait plutôt être considéré comme un développement sur le WWW : un réseau de données qui serait aussi équitable que possible.

Prochaines étapes

\- Les prochaines étapes pragmatiques pour l'EOSC sont d'aligner toutes les parties déjà impliquées dans sa mise en œuvre, d'engager plus de parties, d'embarquer, et d'exploiter dans d'autres domaines les similitudes de l'EOSC avec les activités.

\- L'une des principales activités d'engagement consiste à mettre en place un forum efficace des parties prenantes (tel que le registre des futurs utilisateurs de l'EOSC), car l'élaboration d'un système est nécessaire pour permettre à toutes les parties prenantes de participer à la co-conception de l'EOSC.

\- La prochaine définition de « qu'est-ce que l'EOSC ? » doit être prévue.

\- À long terme, les utilisateurs de l'EOSC doivent être formés.

\- Le besoin d'infrastructures pour le stockage, le calcul et la connexion a été mentionné.

Mercredi, 16 juin 2021

Gouvernance de l'EOSC et association EOSC

14:00-16:00 Carrières et formations des chercheurs

Présidente de session

Marialuisa Lavitrano

Université Milano-Bicocca & Directrice, Association EOSC

Introduction

Au cours de cette session, les trois task forces (TF) du groupe consultatif (AG) « Carrières et formations des chercheurs » ont présenté leurs projets de chartes : la TF  *Programmes de formations et carrières en administration des données*, la TF *Carrières de la recherche, reconnaissance et valorisation*, et la TF *Développement des compétences nationales pour une intégration dans l'EOSC*. Ensemble, ces TF abordent les objectifs opérationnels 003 et 008 du SRIA, qui sont (003) l'intégration croissante des compétences de science ouverte dans les organismes de recherche européens, en passant par l'adoption de programmes d'études et de cadres de formation liés à l'intendance des données pendant la durée du partenariat, et (008) la co-conception et l'adoption d'un cadre de récompenses et de reconnaissance pour les pratiques FAIR et les données ouvertes dans la recherche pendant la durée du partenariat.

Visionner la session complète

Principaux points à retenir

\- La crise de la reproductibilité de la recherche scientifique fragilise la confiance dans la science. Un grand changement culturel incluant les politiques ORDM et FAIR est nécessaire pour changer cela, et le travail est déjà en cours.

\- La science ouverte peut être encouragée et récompensée en développant des évaluations académiques FAIReR qui sont ancrées à la fois dans les directives FAIR pour la gestion des données et dans les politiques d'évaluation responsable de la recherche (FAIReR = FAIR + Responsable). Un changement culturel et une infrastructure technique sont tous deux nécessaires à cet effet. L'EOSC pourrait jouer un rôle important à cet égard.

\- Il y a un besoin croissant de compétences en matière de gestion des données de la recherche et de principes FAIR dans le domaine scientifique, mais actuellement le nombre de gestionnaires de données disponibles n'est pas suffisant.

\- Sur le plan pratique, les coordinateurs de formation en science ouverte font un travail important pour former les pays à s'engager dans l'EOSC et pour créer les formations en science ouverte les plus ouvertes possible (par exemple, en les rendant disponibles dans la langue locale, via des plateformes en ligne, à travers la collaboration et le partage des meilleures pratiques, etc.).

Prochaines étapes

\- Des données aussi quantitatives que qualitatives sont nécessaires pour évaluer la qualité des résultats de la recherche.

\- Le système de récompense académique doit gratifier les pratiques de science ouverte.

\- L'EOSC peut jouer un rôle important dans le changement culturel en rendant la science ouverte et les données FAIR plus populaires et, en fin de compte, à en faire la norme, ainsi qu'à fournir l'infrastructure nécessaire pour moderniser le système de récompense académique.

\- La formation à la science ouverte et l'amélioration des compétences en matière de gestion des données de la recherche et de pratiques FAIR sont d'une importance stratégique pour l'EOSC. L'association EOSC devrait mettre en relation les TF de l'AG « Carrières et formations des chercheurs » avec les nombreuses initiatives et réseaux déjà existants.

Jeudi 17 juin 2021 -- Priorités de mise en œuvre

9h00-10h45 Engagement et collaboration bénéfique entre les infrastructures de recherche et l'écosystème de l'EOSC

Présidentes de session

Suzanne Dumouchel

Huma-Num (CNRS) & directrice, association EOSC

Mirjam van Daalen

Paul Scherrer Institute et présidente, ESFRI Task Force de l'EOSC

Introduction

Les infrastructures de recherche (RI) et leurs communautés thématiques sont des parties prenantes clés de l'EOSC, à la fois en tant que fournisseurs de données et de services thématiques de qualité et en tant qu'utilisateurs des données et services génériques de l'EOSC. Les communautés de RI sont également responsables du changement de culture en matière de données, en encourageant le partage de données « FAIR » de qualité, ainsi que l'ouverture de leur accès. Au cours de cette session, les aspects liés à l'utilisateur et à la co-création de l'EOSC dans sa deuxième phase après 2020 ont été abordés. La nécessité d'un engagement plus étroit et de l'intégration des RI et d'autres communautés thématiques dans l'EOSC, afin de répondre et de donner la priorité aux besoins réels de la communauté de recherche et de fournir une valeur ajoutée à leurs utilisateurs finaux a été soulignée.

Visionner la session complète

Principaux points à retenir

Les RI thématiques et leurs communautés sont des parties prenantes clés pour l'EOSC, son adoption et sa pérennisation (les RI sont des fournisseurs de données et de services de qualité thématiques et sont aussi des utilisateurs de données et de services horizontaux de l'EOSC). Plus la fédération des RI thématiques dans l'EOSC est large et les services horizontaux utilisés, plus l'EOSC a de chances de devenir durable.

\- Alors que les *clouds* constituent la base technique qui rend la mise en place de l'EOSC possible, les ESFRI et les clusters scientifiques sont de leur côté les garants de l'aspect scientifique de l'EOSC et forment ensemble le cadre global dans lequel celui-ci évolue.

\- Les clusters scientifiques :

-   agissent comme des interfaces majeures entre les communautés scientifiques, leurs infrastructures et l'EOSC ;

-   fournissent des données FAIR, élaborent et maintiennent des services communautaires clés ;

-   mettent les services communautaires des clusters à la disposition de la communauté scientifique dans son ensemble ;

-   appliquent les services communautaires à travers les clusters pour de nouvelles recherches et d'importants défis sociétaux ;

-   fournissent des plateformes pour l'interopérabilité scientifique au sein de l'EOSC ;

-   accompagnent les chercheurs dans leur formation sur la science ouverte dans/avec l'EOSC.

\- Les clusters scientifiques de l'ESFRI seront connectés par le biais du projet EOSC Future. Dans ce projet :

\- les domaines scientifiques couvrent des sujets variés comme les impacts du changement climatique sur la biodiversité, l'environnement et les sociétés pour une meilleure compréhension de notre univers et de tous les aspects de la recherche sur la Covid-19 ;

\- la valeur ajoutée scientifique est apportée par de nouvelles analyses interdisciplinaires provenant de RI complémentaires, ce qui était impossible auparavant ;

\- des algorithmes et des méthodes innovants seront partagés avec d'autres communautés scientifiques et avec la société au sein de la plateforme de l'EOSC, comme la gestion innovante de l'accès, de nouvelles approches pour le partage, l'analyse et la réutilisation des données d'imagerie, et le *machine learning*.

\- Les clusters scientifiques de l'ESFRI attendent de l'EOSC :

\- une oreille attentive pour les communautés de l'ESFRI afin que l'utilisation de l'EOSC soit possible et utile pour les chercheurs ;

\- des services de base fiables et faciles d'utilisation pour tous les utilisateurs, tels que l'AAI, la recherche de données, le transfert de données, l'accès au stockage et au calcul, qui fonctionnent de manière transparente dans toutes les disciplines ;

\- une plateforme durable à laquelle les chercheurs peuvent faire confiance et sur laquelle ils peuvent investir du temps (et de l'argent) pour construire leurs flux de travail.

Prochaines étapes

\- *Blue-Cloud et FNS-Cloud : une synergie pour renforcer la recherche ouverte dans le domaine de la pêche* :

\- Blue-Cloud coopère avec le projet FNS-Cloud depuis avril 2020, en raison des points communs partagés par les initiatives sur l'intégration des données sur le cloud thématique *European Open Science Cloud* (EOSC) et l'encouragement des méthodes pour rendre les données plus FAIR (Faciles à trouver, Accessibles, Interopérables, Réutilisables).

\- Cette collaboration a permis d'obtenir des résultats importants dans un laps de temps relativement court. Blue-Cloud et FNS-Cloud soutiennent le développement du nouveau jeu de données uFish de l'Organisation des Nations unies pour l'alimentation, un tableau de référence des composants des produits aquatiques alimentaires très utilisé et cité. Les données proviennent de publications sélectionnées et sont soumises à un processus d'examen et de validation approfondi qui doit être reproduit dans l'application.

\- *SSHOC : Social Science and Humanities Open Cloud* :

\- Le SSHOC construit le SSH Cloud : il faut créer la partie sciences humaines et sociales (SSH) de l'European Open Science Cloud (EOSC) afin de maximiser la réutilisation des données par le biais de la science ouverte et des principes FAIR (normes, catalogue commun, contrôle d'accès, techniques sémantiques, formation). Il interconnecte les RI avec des infrastructures existantes et nouvelles (infrastructure cloud en cluster). Il met également en place la gouvernance en établissant un modèle de gouvernance approprié pour le SSH-EOSC.

\- Le SSHOC propose des formations et des supports de formation en ligne et hors ligne, ainsi qu'un réseau international de formateurs pluridisciplinaires pour soutenir les communautés. Il propose un Open Marketplace pour les sciences humaines et sociales facile à utiliser, à l'échelle de l'UE, où les outils et les données sont accessibles gratuitement. Ces outils sont de bonne qualité et « *cloud ready* ». Les données sur les SSH sont de bonne qualité également, et les mécanismes d'accès fiables et sécurisés pour ces données sont disponibles, conformément aux exigences légales et nationales/institutionnelles de l'UE.

*- EOSC-Life : les services de l'EOSC pour les services des domaines transversaux en sciences de la vie :*

\- Les besoins des utilisateurs, des développeurs et des administrateurs d'infrastructures sont à l'origine des écosystèmes ELIXIR et EOSC-Life Tools. Ils ont été conçus en collaboration avec les utilisateurs d'EOSC-Life et comprennent des services et des technologies transversaux et horizontaux au service des parties prenantes hors du domaine du vivant, des données thématiques, des outils, des ontologies et des sorties d'objets numériques. Les services pratiques et axés sur des cas d'utilisation réels des RO peuvent aider à connecter les ressources disponibles via d'autres services de l'EOSC et des RI connexes.

Jeudi 17 juin 2021 -- Priorités de mise en œuvre

9h00-10h45 Engagement et collaboration bénéfique entre les infrastructures de recherche et l'écosystème de l'EOSC

Présidentes de session

**Suzanne Dumouchel**

Huma-Num (CNRS) & directrice, association EOSC

**Mirjam van Daalen**

Paul Scherrer Institute et présidente, ESFRI Task Force de l'EOSC

Prochaines étapes

\- *ESCAPE - Multiplier la participation à l'EOSC par un facteur mille : la science citoyenne dans l'EOSC :*

\- Le public amateur de sciences est à la fois le groupe le plus grand et le plus oublié des parties prenantes de l'EOSC. Rendre les données FAIR est plus facile que les rendre utiles. Les scientifiques citoyens doivent également s'impliquer. Il est donc nécessaire d'élaborer un plan réaliste pour que les scientifiques citoyens et l'EOSC en retirent véritablement des avantages réciproques.

\- *Engagement des chercheurs : Visions, besoins et exigences pour les environnements de recherche (futurs) :*

\- Les obstacles à l'engagement des chercheurs dans l'EOSC sont la saturation, le syndrome des habits de l'empereur, le trop-plein d'informations et le fait que cet engagement soit considéré comme « ce n'est pas mon affaire ». Ainsi, plusieurs enseignements ont été tirés des activités menées : Les chercheurs veulent des informations sur des services et des avantages concrets pour la recherche quotidienne ; il faut trouver des moyens de communiquer clairement sur ce qu'est l'EOSC ou ce qu'il peut offrir ; les informations doivent être préparées de manière à ce que les chercheurs sachent où les trouver et puissent les utiliser ; ce qui n'existe pas encore ne peut pas facilement se vendre à un public critique ; et en général, la démarche sincère, qui consiste à demander aux chercheurs ce dont ils ont besoin et ce qu'ils veulent, a été très bien accueillie.

Jeudi 17 juin 2021

Priorités de mise en œuvre

11h15-13h00 Pérennisation de l'EOSC

Président de la session

**Bob Jones**

CERN & directeur, association EOSC

**Introduction**

La pérennisation est la clé du succès à long terme de l'EOSC. Elle couvre de nombreux aspects, mais deux domaines ressortent plus que les autres : la viabilité financière et la conservation durable à long terme des données et autres objets numériques pertinents pour la recherche. Les travaux des task forces de l'association EOSC sur les modèles de financement et sur la conservation à long terme des données seront essentiels à cet égard. Les projets de l'EOSC développent une gamme de services, cependant, pour que ceux-ci soient durables à long terme, ils doivent aller au-delà du financement par projet. Les fournisseurs commerciaux auront un rôle clé dans la fourniture des services de l'EOSC, et la pérennisation de ceux-ci sera au cœur de tout processus de planification commerciale.

Visionner la session complète

**Principaux points à retenir**

\- La définition de la pérennisation ne doit pas seulement couvrir l'aspect financier, mais aussi par exemple la conservation numérique à long terme. Les task forces (TF) de l'association EOSC qui composent le groupe consultatif (AG) sur la pérennisation devront avoir un rôle transversal avec les autres task forces.

\- La dépendance à l'égard d'un financement à court terme, basé sur des projets, n'est pas compatible avec la pérennisation à plus long terme de l'EOSC. Les projets de l'EOSC développent une série d'exemples de la manière dont la pérennisation peut être atteinte.

\- Les services de l'EOSC devraient être « gratuits au point d'utilisation », cependant, cela nécessitera une série de mécanismes pour couvrir le coût du développement et du maintien de ces services durables à long terme.

\- Une composante essentielle de l'EOSC sera constituée de services fournis par des organisations commerciales, tels que les services commerciaux en cloud. Un des mécanismes permettant de fournir ces services gratuitement au point d'utilisation est le « chèque-service » fourni aux chercheurs individuels pour l'achat de ces services. Cependant, des problèmes importants affectent le flux transfrontalier des ressources financières pour couvrir ces coûts.

**Prochaines étapes**

\- Les travaux des task forces de l'association EOSC sur les modèles de financement et sur la conservation à long terme des données seront essentiels pour assurer la pérennisation à long terme de l'EOSC.

\- Les projets de l'EOSC développent une gamme de services, mais pour qu'ils soient durables à plus long terme, ils doivent aller au-delà du financement par projet. Il existe un certain nombre d'exemples, issus de projets tels que EOSC Enhance, DICE et CS3MESH4EOSC, qui montrent comment la pérennisation peut être atteinte.

\- Les fournisseurs commerciaux auront un rôle clé dans la fourniture des services de l'EOSC, et la pérennisation de ceux-ci sera au cœur de tout processus de planification commerciale. Des projets tels que Archiver, OCRE et CloudBank explorent comment fournir des services commerciaux sur une base durable, qui sont « gratuits au point d'utilisation ».

\- Des travaux sont en cours pour étudier les modèles commerciaux potentiels pour soutenir la pérennisation, et EOSC Hub a identifié une série de modèles commerciaux couvrant l'approvisionnement, l'accès virtuel, les ressources en nature et/ou en espèces et la coopération public-public. Le projet OntoCommons.eu a identifié que la FAIRisation des données est un service « payant » que l'EOSC pourrait offrir aux producteurs de données commerciales afin de les valoriser.

Jeudi 17 juin 2021

Priorités de mise en œuvre

13:30-14:00 Déjeuner & EOSC-Clinic : session de questions ouvertes

Présidents de session

**Ronan Byrne**

HEAnet & Directeur, Association EOSC

**Suzanne Dumouchel**

Huma-Num (CNRS) & Directrice, Association EOSC

**Introduction**

L'EOSC-Clinic était une partie essentielle du Symposium de l'EOSC, permettant aux participants de poser des questions ouvertes via sli.do et d'interagir avec les membres du conseil d'administration de l'EOSC. Les participants à la session ont eu l'occasion de réfléchir à la pérennisation de l'EOSC, qui a été discutée lors de la session précédente, et à sa pertinence au niveau du marketing pour l'avenir. Ils ont également abordé les prochaines étapes en relation avec GAIA-X, les plans de gestion des données qui seront établis au sein d'Horizon Europe et de l'EOSC, et l'initiative FAIR forever.

Visionner la session complète

**Principaux points à retenir**

\- Les fournisseurs commerciaux sont un bon complément à l'offre de services de l'EOSC s'ils sont adoptés à bon escient et pour certains services.

\- Le marketing est nécessaire pour faire passer le message concernant l'EOSC avant que celui-ci ne soit capable de se commercialiser lui-même.

\- Les modèles commerciaux peuvent inclure un approvisionnement basé sur l'échange avec les fournisseurs existants et la négociation, et pas seulement sur l'argent. Le marché n'apporte pas tout.

\- Plus les données sont sauvegardées, plus le risque qu'elles deviennent inutilisables est élevé. La gestion des données à long terme doit être prise en compte dans la planification et les audits.

**Prochaines étapes**

\- Une meilleure promotion commerciale de l'EOSC est nécessaire

\- La gestion des données à long terme ne doit pas concerner uniquement la sauvegarde des données (à long terme) mais aussi le fait de trouver les données (à long terme).

Jeudi 17 juin 2021

Priorités de mise en œuvre

14:00 -- 16:00 Difficultés techniques de l'EOSC

**Président : Klaus Tochtermann**

ZBW & directeur, association EOSC

**Introduction**

Au cours de cette session, les projets de chartes des task forces (TF) pour le groupe consultatif (AG) « Problématiques techniques de l'EOSC » ont été présentés par les membres des groupes de rédaction, à savoir *Infrastructure pour des logiciels de recherche de qualité* (présenté par **Cerlane Leong**, CSCS), *Architecture AAI* (présenté par **Christos Kanellopoulos**, GÉANT), et *Interopérabilité technique des données et services* (présenté par **Joan Masó**, UAB). « Problématiques techniques de l'EOSC » est l'un des AG de l'association EOSC. Les TF au sein de cet AG se concentrent sur la mise en œuvre de l'architecture technique et de l'interopérabilité dans l'EOSC. L'AG donne également des indications sur les domaines stratégiques pour les travaux futurs.

Les principales priorités suivantes ont été mises en évidence pour les task forces et les sessions en petits groupes qui ont suivi.

\- La **TF Architecture** **AAI** s'appuiera sur l'excellent travail qui a déjà été réalisé. Elle développera la prochaine version de l'architecture AAI de l'EOSC, s'engagera avec les parties prenantes pour identifier les nouveaux cas d'utilisation et les exigences, et analysera les modèles de gouvernance pour l'AAI de l'EOSC.

\- La **TF Infrastructure pour des logiciels de recherche de qualité** explorera le paysage des logiciels communautaires, définira ce qu'est un logiciel de qualité pour l'EOSC, mettra en œuvre une infrastructure scientifique pour les logiciels de recherche dans l'EOSC et assurera la pérennité des logiciels lorsque le financement sera épuisé.

\- La **TF Interopérabilité technique des données et services** s'appuiera sur le cadre d'interopérabilité de l'EOSC (EIF). Elle finalisera les principes directeurs de l'interopérabilité, analysera les systèmes et les normes d'interopérabilité existants, encouragera l'alignement entre les normes de l'EOSC et les principales activités liées à l'industrie, classera les catégories de services en incluant les perspectives des différentes parties prenantes, identifiera et spécifiera un ensemble minimal de fonctionnalités et encouragera la consommation des normes et des services d'interopérabilité de l'EOSC.

Visionner la session complète

**Sous-groupe de discussion : architecture AAI**

**Présidente : Marina Adomeit**

SUNET

\- La réunion a commencé par une « analyse rétrospective » des principaux résultats du projet EOSC-hub et s'est terminée par une « projection » sur les aspirations et les principaux travaux à réaliser dans le cadre du projet EOSC Future.

\- **Le projet EOSC-hub (Nicolas Liampotis, GRNET & EOSC-Hub/EOSC Future**) a pris en compte les exigences des utilisateurs finaux et des fournisseurs de ressources afin de garantir que l'AAI soit aussi transparente que possible. Il a permis de mettre en évidence un système de fonctionnement dans les communautés de recherche et la fourniture d'un service AAI en marque blanche pour le portail de l'EOSC. Cependant, il y a des lacunes évidentes dans le travail qui, nous l'espérons, seront comblées par le projet EOSC Future.

\- **Le projet Umbrella (Jean-François Perrin, ESRF & PaNOSC)** a permis de créer une grande et diverse communauté de 50 000 utilisateurs ayant des connaissances informatiques très limitées. UmbrellaID est l'AAI de la communauté Photon et Neutron depuis 2012. Au cœur de ce projet se trouvait un identifiant unique, commun et pérenne.

\- **ENVRI FAIR (Keith Jeffery, ENVRI & EPOS-ERIC)** a mis en place une task force AAA (authentification, autorisation, comptabilité) pour réaliser la mise en œuvre de l'AAAI au sein des sous-domaines des RI et du ENVRI-Hub, bien qu'ils ne visent pas à construire un fournisseur d'identité centralisé. Le développement des politiques EPOS est fondamental même si c'est une entreprise énorme.

\- L'**EOSC Future (Christos Kanellopoulos, GÉANT & EOSC Future)** reprend là où EOSC-hub s'est arrêté et se concentre sur l'intégration des AAI communautaires, des fournisseurs d'identité et des services. D'ici le 30e mois (2023), un chercheur devrait être en mesure d'entreprendre le cycle de vie complet de la recherche, c'est-à-dire l'intégration transparente de l'AAI communautaire à la fédération AAI de l'EOSC par le biais d'un embarquement en libre-service et d'un accès intersectoriel à la fédération de l'EOSC. L'AAI de l'EOSC rassemblera toutes les entités et tous les composants pour un accès sans faille.

**Sous-groupe de discussion : infrastructure pour des logiciels de recherche de qualité**

**Présidente : Cerlane Leong**

CSCS

\- **La plateforme SQaaS (Pablo Orviz, Conseil national de la recherche espagnol (CSIC) & EOSC-Synergy),** développée au sein de EOSC Synergy, est utilisée par les services thématiques afin de composer des éléments ad hoc en fonction des besoins et couvre différents services. La bibliothèque est le composant central. Elle met en œuvre les critères de qualité, est conviviale et ne nécessite pas de connaissances techniques.

\- **Towards Reusable Research Software (Daniel Garijo, Universidad Politécnica de Madrid)** : les métadonnées des logiciels sont abondantes mais ne sont généralement pas lisibles par machine. Les graphes de connaissances peuvent être utilisés pour relier le logiciel de recherche et ses composants. Les logiciels de recherche doivent être exploitables et utiles pour comprendre les différences, la paternité, la portabilité, la comparaison et réduire le temps nécessaire à l'adoption. Il est donc très important de rendre les métadonnées des logiciels de recherche exploitables par les machines.

\- **Environnement de pré-production NI4OS-Europe (Dusan Vudragovic & Sonja Filiposka FCSE, UKIM & NI4OS-Europe)** : NI4OS-Europe développe un environnement de pré-production pour garantir un ensemble minimum de services fédérateurs, pour vérifier la qualité des services avant l'intégration de l'EOSC et pour soutenir les fournisseurs de services au niveau régional. Il dispose de tous les éléments essentiels permettant l'intégration de services génériques et thématiques dans l'écosystème de l'EOSC.

\- OSF **et amélioration de l'expérience du chercheur (Nici Pfeiffer & Eric Olson, Center for Open Science)** : la mission de cet outil est d'améliorer l'ouverture, l'intégrité et la reproductibilité de la recherche, changeant ainsi la culture de la recherche. L'environnement permet aux développeurs de recherche de tester l'intégration de leurs ressources. Il leur permet d'éviter de reproduire des fonctionnalités et des outils déjà existants. Au contraire, ils sont en mesure de les rénover, d'innover et de partager des logiciels avec leurs pairs et les chercheurs.

\- **EGI ACE/EGI Application Database (William Karageorgos, IASA, EGI- ACE)** est un service central pour le stockage d'informations sur les solutions logicielles, impliquant des programmeurs, des scientifiques et des publications. En tant que catalogue de logiciels, il constitue un système de classification, fournit des informations sur les sites pris en charge et contient des informations sur les versions des logiciels et sur les auteurs des publications, ainsi que des références croisées entre les entrées.

**Sous-groupe de discussion : défis de l'interopérabilité pour les communautés thématiques**

**Président : Joan Masó**

UAB

\- **ENVRI-hub (Andreas Petzold, Institute of Energy and Climate Research & ENVRI-FAIR)** est la plateforme de la communauté des sciences environnementales au sein de l'EOSC. Les chercheurs travaillent déjà individuellement via l'accès direct aux portails d'infrastructure de recherche. Bientôt, l'accès aux sous-domaines sera ajouté en tant que service, avec des projets futurs pour intégrer et coordonner l'accès de tous les chercheurs aux ressources de l'ENVRI, via le ENVRI-Hub. Il peut servir de modèle à l'architecture de l'EOSC.

\- **L'écosystème NEANIAS (Georgios Kakaletris, CITE & NEANIAS)** a présenté les principes de conception, l'architecture et les ressources d'infrastructure de la plateforme. NEANIAS est confronté à deux défis majeurs en matière d'interopérabilité : l'interopérabilité des services de bout en bout (authentification, autorisation et délégation, et confiance inter-domaines) et la résolution et l'accès aux données en un point unique, exploitable par une machine (un service PID et un support multi-protocole seraient nécessaires).

\- **CoVis : a curated knowledge map of seminal works on COVID-19 (Peter Kraker, Open Knowledge Maps)** utilise des cartes de connaissances pour structurer les quantités gigantesques de données à propos de la Covid-19 (200 000 articles l'année dernière), aidant ainsi les scientifiques à obtenir de meilleurs résultats de recherche et à gagner du temps pour les découvertes scientifiques. Le projet a démarré avec l'aide du financement de la co-création EOSCsecretariat.eu et collabore avec OpenAIRE et TRIPLE.

\- **ESCAPE (Mark Allen, CNRS & ESCAPE)** est un partenariat de recherche en astronomie et en physique des particules pour la science ouverte. Son architecture comprend un observatoire virtuel, un lac de données (bien qu'il s'agisse davantage d'une structure fédérée que d'un entrepôt de données) et une plateforme scientifique axée sur la science citoyenne. La connexion à l'EOSC est en cours.

\- **WfExS : un composant logiciel pour permettre l'utilisation de RO-Crate dans la collaboration d'outils EOSC-Life (Laura Rodríguez-Navas, BSC & EOSC-Life)** est un backend de service d'exécution de flux de travail de haut niveau, développé dans le cadre d'un démonstrateur EOSC-Life (D7), utile pour gérer les flux de travail dans différents domaines. Il est conçu pour faciliter l'analyse de données sensibles à l'aide d'infrastructures existantes et met l'accent sur l'analyse reproductible et réplicable en utilisant des objets numériques comme RO-Crate, en utilisant un seul document de métadonnées liées aux données pour décrire l'objet numérique. Il peut être appliqué pour gérer le cycle de vie de la recherche et pourrait servir de modèle à l'EOSC.

**Sous-groupe de discussion : défis transversaux de l'interopérabilité**

**Président : Juan Bicarregui**

STFC & UKRI

\- **Fenix : un modèle pour les futures couches de services d'infrastructure numérique impliquant le HPC (Dirk Pleiter, KTH / Forschungszentrum Juelich)** fournit des services d'infrastructure fédérés pour soutenir les services de plateforme spécifiques à la communauté. Il s'agit d'un effort à long terme des centres de supercalcul pour fournir une variété de services d'infrastructure numérique et il vise à soutenir diverses communautés scientifiques et d'ingénierie dans le déploiement de services de plateforme spécifiques à un domaine.

\- **Permettre la composabilité de ressources dans l'EOSC (Diego Scardaci, EGI Foundation & EOSC Future)** est un rôle essentiel que l'EOSC Future jouera pour permettre la composition de ressources à travers les infrastructures en fournissant des API et des métadonnées, des programmes d'interopérabilité et des capacités de portail. À la fin du projet, des  « indicateurs de composabilité » seront associés aux ressources de l'EOSC, les chercheurs pourront accéder à des flux de travail entièrement intégrés/de bout en bout pour divers sujets de recherche, et il y aura un programme d'exécution.

\- **Directives d'interopérabilité d'EOSC-hub (Giacinto Donvito, INFN & EOSC- hub)** : Le projet a établi des directives d'interopérabilité pour l'EOSC en utilisant une architecture technique de référence. Six lignes directrices concernent la fédération de services (EOSC-Core) et douze autres les services communs/horizontaux (EOSC-Exchange). Les multiples exemples de services composés résultant des directives d'interopérabilité d'EOSC-hub constituent un apport important pour le travail de l'EOSC Future sur le programme d'interopérabilité.

\- **Programme d'interopérabilité des services (Leah Riungu-Kalliosaari, CSC - IT Center for Science & EOSC-Nordic)** : l'EOSC-Nordic a considéré le programme d'interopérabilité européen (plus concerné par les services publics que par les services informatiques interopérables) et le cadre d'interopérabilité de l'EOSC (plus concerné par les données) pour trouver des recommandations qui amélioreraient l'interopérabilité des services entre les fournisseurs de services, en considérant la diversité des fournisseurs et des services eux-mêmes. En utilisant des cas d'utilisation de différentes communautés de recherche, l'EOSC-Nordic a interrogé les fournisseurs de services et a produit un rapport de 18 pages disponible sur son site web.

\- **ScienceMesh (Hugo Labrador, CERN & CS3MESH4E0SC)** vise à construire un maillage interopérable distribué de sites tirant parti des services existants de synchronisation et de partage de fichiers. Il compte déjà 400 000 utilisateurs, 16 Po de données et plus de 130 organisations dans 25 pays. Son fonctionnement est décentralisé et les sites sont souverains. ScienceMesh fournit une IOP (plateforme d'interopérabilité) pour établir rapidement la communication et rejoindre immédiatement l'écosystème ScienceMesh.

**Sous-groupe de discussion: défis de l'interopérabilité pour augmenter l'offre de services de l'EOSC**

**Président : Anastas Mishev**

UKIM

\- **DICE (Nadia Tonello, Barcelona Supercomputing Center & DICE)** exploite des cas d'utilisation d'intégration de services génériques dans des plateformes communautaires, dans le but d'attirer de nouveaux utilisateurs, de créer une composabilité des services de données et d'améliorer l'utilisation interdisciplinaire des données.

\- **OpenAIRE CONNECT Research Community Dashboard** **(Alessia Bardi, CNR - ISTI / OpenAIRE & OpenAIRE Nexus)** fournit des passerelles de recherche ouvertes configurables et réduit les obstacles à l'adoption des pratiques de publication de la science ouverte dans la communauté de recherche.

\- **EGI-ACE (Mark Dietrich, EGI Foundation & EGI-ACE)** intègre les espaces de données, l'écosystème de données et les initiatives de données de diverses communautés en harmonisant leurs modèles et en permettant une gouvernance et des normes d'interopérabilité entre les communautés. Il crée un cadre commun, avec des plans pour étendre la collaboration et affiner le programme en trouvant des points communs avec davantage de communautés.

\- **OpenAIRE (Andrea Mannocci, ISTI - CNR & OpenAIRE-Nexus)** vise à construire un cadre d'interopérabilité de type « grille scientifique ouverte » (OSG), dans lequel le catalogue de ressources de l'EOSC circule de manière transparente avec des informations provenant de toutes les parties prenantes concernées. L'EOSC peut capitaliser sur les synergies. OpenAIRE prévoit de lancer un groupe d'intérêt RDA sur « les OSG pour les données FAIR » et des groupes de travail associés.

\- **RELIANCE (Raul Palma, Poznan Supercomputing and Networking Center & RELIANCE)** vise à étendre les capacités de l'EOSC avec un support amélioré pour les activités de recherche et les services interconnectés, en accord avec le cadre d'interopérabilité de l'EOSC, en utilisant les objets de recherche comme outils clés qui soutiendront et apporteront un changement systématique aux pratiques de science ouverte au sein de l'EOSC. RELIANCE prévoit de faire la démonstration de ses services par le biais de trois communautés des sciences de la Terre et prévoit d'engager d'autres communautés par le biais d'un appel ouvert.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Vendredi 18 juin 2021 - Priorités de mise en œuvre

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Vendredi 18 juin 2021

Priorités de mise en œuvre

9:00 -- 10:45 Mise en œuvre de l'EOSC

**Présidente : Suzanne Dumouchel**

Huma-Num (CNRS) & Directrice, association EOSC

**Introduction**

Le groupe consultatif (AG) et les task forces (TF) sur la mise en œuvre de l'EOSC se concentrent sur la manière de déployer les recommandations de l'EOSC et de tester leur applicabilité avec les communautés de recherche et les fournisseurs de services. Ils cherchent également à promouvoir une adoption plus large de l'EOSC, en particulier au sein de la communauté des chercheurs. Cette session s'est concentrée sur les activités actuelles dans ces domaines avec des contributions de la communauté par le biais de l'appel à contributions ouvert. Une réponse animée de la communauté a amené le Comité de programme à organiser quatre sessions en petits groupes.

Visionner la session complète

**Session principale**

\- Trois TF ont été établies au sein de l'AG sur la mise en œuvre de l'EOSC. Leur diversité doit être le principe prioritaire. Dans une approche ascendante, l'EOSC doit passer des recommandations qui ont été produites précédemment à la phase de mise en œuvre.

\- Les principaux objectifs de la **TF « Intégration et adoption des chercheurs »** sont d'identifier les besoins des chercheurs et de leur permettre de s'approprier l'EOSC. Les activités principales de cette TF sont la communication et la formation.

\- Les principaux objectifs de la **TF « Politique et mise en œuvre des identifiants pérennes (PID) »** sont de contribuer à la prochaine version du SRIA de l'EOSC concernant les infrastructures de PID, les PID exploitables par machine, le méta résolveur, le graphe PID, les PID et la gestion des données FAIR, la qualité de service pour les PID et les nouvelles technologies des PID. Elle fournira ensuite les commentaires de la communauté et fera des recommandations pour l'intégration des services PID dans l'écosystème de l'EOSC, couvrant tous les types de ressources de l'EOSC, rédigera une recommandation sur la résolution globale des PID, y compris les instances existantes et émergentes de méta-résolveurs, publiera un rapport sur les formats de données et leurs définitions, produira un rapport sur le graphe PID de l'EOSC, définira les critères d'évaluation de la qualité des PID et les fera répertorier sur la MarketPlace de l'EOSC, et collectera les cas d'utilisation des PID illustrant la gestion des données FAIR et les décrira dans un rapport.

\- La **TF « contrôle de conformité des règles de participation »** doit garantir que les conditions d'entrée de l'EOSC évoluent des règles de participation (RoP) actuelles de haut niveau à des critères pratiques de mise en œuvre dans l'EOSC. Elle doit également définir à quel niveau les RoP peuvent être raisonnablement contrôlées, et mettre en place un cadre pour permettre ce contrôle aux côtés d'autres parties prenantes clés, et impliquer la communauté dans la définition des RoP. Les activités principales comportent trois phases : L'architecture des RoP (structure des règles), des RoP étendues qui fournissent des critères ou des exigences plus spécifiques pour les fournisseurs et les ressources, ainsi qu'un plan de surveillance, un premier groupe « d'appel de l'EOSC », et une proposition de mise en place d'un conseil des RoP à l'AG et à l'association EOSC.

**Sous-groupe de discussion : Règles de participation et politique des PID**

**Président : Juan Bicarregui**

STFC & UKRI

\- **Règles de participation pour l'EOSC dans le contexte de NI4OS-Europe (Dusan Vudragovic, Institut de physique de Belgrade & NI4OS-Europe)** : les principes FAIR et l'architecture de l'EOSC imposeront des contraintes sur les RoP. NI4OS a identifié un niveau d'intégration minimal entre l'EOSC-Core et l'EOSC-Exchange. Trois domaines en particulier ont été identifiés concernant le niveau de mise en œuvre : le niveau de maturité technologique, le niveau d'intégration d'EOSC-Core et le niveau d'intégration de la gestion ; chacun d'entre eux comporte neuf niveaux de catégorisation des ressources. Lorsqu'ils sont combinés ensemble, ils produisent un indicateur du niveau cumulatif d'intégration d'un service au sein de l'EOSC. Il est crucial de s'assurer que les ressources sont alignées sur la qualité de la recherche entreprise.

\- **RoLECT** **: un outil d'auto-évaluation de la conformité juridique et éthique aux RoP de l'EOSC (Marianna Katrakazi, ATHENA RC & NI4OS Europe) :** il se concentre sur les aspects juridiques et éthiques de la conformité aux RoP de l'EOSC, et est conçu pour promouvoir la conformité et identifier la non-conformité. La mise en œuvre des RoP donne lieu à de nombreux défis et l'objectif est d'aider les fournisseurs de ressources. L'outil vise à soutenir ces fournisseurs qui ne disposent pas de l'expertise juridique/éthique nécessaire pour évaluer leur propre conformité de manière indépendante, grâce à un flux structuré de questions classées en trois niveaux d'importance. L'outil est dynamique et les questions posées dépendent des réponses précédentes. Il peut être exploré en tant qu'utilisateur invité\[2\] et comprend un outil d'autorisation de licence pour vérifier la compatibilité entre différentes licences ouvertes.

\- **Politique et mise en œuvre des PID de l'EOSC (Mark van de Sanden, EUDAT) **: l'interaction de la politique des PID de l'EOSC a été examinée avec les services de l'EUDAT qui sont touchés : B2HANDLE, B2SAFE, B2SHARE, B2FIND, ainsi que la manière dont les principes clés de la politique des PID peuvent être appliqués à un niveau pratique. Les PID doivent être uniques, pérennes et réglables au niveau mondial. La pérennité peut avoir des significations différentes selon les communautés. Six rôles sont définis dans le cadre de la politique des PID. Cependant, les rôles peuvent s'appuyer sur plusieurs partenaires pour livrer, par exemple, le fournisseur de services de PID. Les principales difficultés sont la traduction de la politique en critères permettant d'évaluer la conformité, l'interopérabilité technique et la pérennisation à long terme des fournisseurs de services de PID.

\- **Que signifie le P de PID ? Pérennité ou Pérennisable ? (Jonathan Clark, The DOI Foundation)** : la pérennité a tendance à être considérée comme acquise. Pérennisable - qui peut être rendu pérenne - être permanent. Cependant, la pérennité n'est pas une propriété inhérente au système. La caractéristique pérennisable doit être intégrée dans la conception. Deux types de pérennité représentent un défi particulier : la pérennité d'un service pour « résoudre » l'identifiant à l'objet et la pérennité d'un service pour permettre la mise à jour de la liaison entre identifiant et objet. La reprise après sinistre et la continuité des activités sont deux questions clés, ainsi que la définition de ce qui doit être fait pour assurer la pérennité.

\- **Les PID ouvertement reproductibles comme facteur de FAIRitude dans les pratiques de partage de données (Andrey Vukolov, ELETTRA Sincrotrone Trieste & ExPaNDS)** : le modèle d'accès n'est pas défini explicitement. Dans le modèle actuel, les données existantes représentant un objet donné sont stockées séparément. Chaque nœud du flux d'administration de PID est un point de défaillance distinct, car toute défaillance technique entraîne une perte de données. En termes de FAIRitude, le PID ne fournit qu'un seul point de terminaison d'adresse pour accéder à un objet numérique, et la RI est séparée de l'autorité de l'espace de nom du PID. Réflexion sur la définition d'un PID reproductible : le préfixe/espace de noms du PID reproductible est défini par un algorithme connu, le PID est reproductible à partir des données/métadonnées elles-mêmes par un algorithme ouvert, un workflow est proposé pour les PID ouvertement reproductibles.

\- **Au-delà des PID : l'infrastructure savante ouverte mondiale (Edward Pentz, Crossref ; Matt Buys, DataCite)** - Bien qu'importants, les PID n'ont pas de « propriétés magiques ». L'accent devrait être mis sur les services et l'infrastructure avec lesquels ils sont connectés. Quels sont les principes qui sous-tendront une infrastructure savante ouverte mondiale ? Il faut se concentrer sur les initiatives ouvertes, gérées par la communauté, en se référant également aux principes de l'infrastructure savante ouverte (POSI), vers un réseau ouvert de relations riche et réutilisable pour soutenir la recherche ouverte et les chercheurs. Il existe 16 principes équilibrés couvrant les domaines de la gouvernance, de la pérennisation et de l'assurance que les organisations doivent s'engager à respecter. Les activités de mise en œuvre de la politique des PID de l'EOSC devraient envisager un alignement avec les POSI.

Priorités horizontales

\- Des méthodologies et des outils d'évaluation existent pour aider à comprendre le niveau d'intégration d'un service au sein de l'EOSC, ainsi que la conformité légale et éthique d'un service. Il est possible de s'appuyer sur ces outils.

\- Assurer une définition claire des rôles et des responsabilités.

\- La recherche ouverte doit être soutenue par une infrastructure savante ouverte mondiale.

\- La fourniture de PID et de services PID durables, pérennes et reproductibles est un élément essentiel d'une telle infrastructure.

\- Envisager la pérennité à long terme des métadonnées, même si les données auxquelles elles se rapportent ne sont plus disponibles.

**Sous-groupe de discussion : engagement communautaire et types de données**

**Président : Anastas Mishev**

UKIM

\- **Services thématiques EOSC-SYNERGY : contribution à l'écosystème de l'EOSC (Ignacio Blanquer, UPV).** Les services thématiques EOSC-Synergy ainsi que leur contribution à l'écosystème de l'EOSC ont été présentés. Les services thématiques de quatre grands domaines de recherche (observation de la Terre, études environnementales, biomédecine, astrophysique) ont été analysés à travers certains aspects techniques, aboutissant à un cycle vertueux de services qui augmentent leur capacité, leur performance, leur fiabilité et leur qualité.

\- **EXPANDS et la communauté scientifique des sources de lumière neutronique et synchrotron (Isabelle Boascaro-Clarke, Diamond Light Source)** ont présenté une vue d'ensemble à la communauté scientifique des sources de lumière neutronique et synchrotron qui englobe 25 000 utilisateurs d'installations photoniques et les encouragent à publier des données de recherche en même temps que leurs articles de recherche.

\- **Fostering Researcher Engagement by Providing Open Research Knowledge Graph (Sören Auer, TIB Leibniz Information Center for Science and Technology)** est le concept d'un graphe de connaissances qui relie les différents éléments d'informations liés à un document et les uns aux autres dans un format graphique. Les graphes de connaissances peuvent être étendus avec des informations bibliographiques.

\- **NFDI4Culture : 3D Heritage Data Interaction and Enrichment (Ina Blümel, Bibliothèque nationale allemande des sciences et de la technologie)**, est un consortium pour les données de recherche sur le patrimoine culturel matériel et immatériel. Ils ont expliqué le défi que représentent les outils de visualisation dans ce domaine et ont fait la démonstration d'un outil d'interaction et d'enrichissement des données du patrimoine en 3D qui peut être utilisé à la fois par les chercheurs et les experts travaillant dans le domaine de la culture.

\- **FiglinQ : une plateforme permettant la création de manuscrits intelligents, interactifs et connectés à des données (Przemek Krawczyk, Amsterdam UMC).** C'est une plateforme permettant de produire des graphiques interactifs qui incluent et affichent les données sur lesquelles ils sont basés. Ces graphiques (y compris les données) peuvent être intégrés directement dans les manuscrits, ce qui permet aux lecteurs de cliquer et d'étudier les données qui se cachent derrière un graphique. On parle alors de manuscrits intelligents.

Priorités horizontales

\- Les services que ces organisations et initiatives fournissent et qui pourraient être ajoutés à l'EOSC ont besoin d'alignement et de fertilisation croisée.

\- L'association EOSC a été considérée comme un très bon canal pour obtenir une plus grande visibilité pour leurs services et outils, dont certains sont assez nouveaux et pas encore très connus (par exemple FiglingQ).

\- Les petites RI nationales devraient être liées à la vision européenne plus large à travers l'EOSC.

\- Le moment est venu de passer d'images statiques dans les articles de recherche à une science véritablement numérique, les graphiques électroniques et interactifs sont sans aucun doute un pas dans cette direction.

\- Il faut adopter une approche entrepreneuriale de l'EOSC et de la science numérique, en passant des discussions de haut niveau et des processus de normalisation aux tests et à l'apprentissage.

\- Le partage d'informations sur les services d'une communauté de recherche à une autre pourrait réserver des bonnes surprises qui ne semblaient pas pertinentes au départ mais qui ont ensuite été adoptées/ont suscité de nouvelles idées, etc. dans d'autres communautés scientifiques (multidisciplinarité).

\- Une plus grande visibilité par le biais de l'EOSC pourrait amener de nouveaux utilisateurs pour les services. En outre, l'interopérabilité est cruciale pour l'engagement des utilisateurs (par exemple, l'authentification unique) et la valeur ajoutée de l'EOSC pour les chercheurs doit être cristallisée pour les intéresser.

\- L'EOSC doit essayer de se projeter dans l'avenir et de penser à ce que les chercheurs souhaitent utiliser. Par exemple, la tomographie devient très lourde en données.

**Sous-groupe de discussion : engagement de la communauté**

**Présidente : Donatella Castelli**

CNR-ISTI & EOSCsecretariat.eu

\- **Mise en œuvre d'un avenir scientifique ouvert : adoption de nouvelles pratiques, incitations, politiques et outils (David Mellor, Center for Open Science)** : un problème d'action collective se pose sur la façon dont la science devrait être faite. Une étude a été réalisée sur le comportement des chercheurs par rapport aux normes de pratique de la science : La plupart des personnes interrogées sont d'accord sur l'importance du partage ouvert ; le scepticisme et l'accent sur la qualité devraient être les normes de la science par opposition au secret, au dogme et à l'accent sur la quantité. Mais la plupart des chercheurs interrogés pensent que les autres chercheurs ne suivent pas les normes de la science ouverte. Un certain nombre de recommandations ont été formulées pour réduire le risque perçu de l'adoption des pratiques de la science ouverte couvertes par l'EOSC : les pratiques doivent être possibles, faciles, normatives, gratifiantes et obligatoires.

\- **Relier l'EOSC aux communautés locales de science ouverte (Loek Brinkman, Open Science Community Utrecht)** : trois défis ont été identifiés pour l'EOSC : sensibiliser, atteindre les utilisateurs et obtenir un retour d'information de leur part. Les chercheurs sont les moteurs du changement culturel, formant des communautés et des réseaux de base (tels que l'INOSC), où les chercheurs partagent leurs expériences et apprennent à intégrer la science ouverte dans leurs pratiques. L'université d'Utrecht encourage de telles communautés locales dans d'autres universités des Pays-Bas et d'autres pays de l'UE. Elle a créé un kit de démarrage pour former et lancer des communautés de science ouverte.

\- **Le programme EOSC Early Adopter et les centres de compétences :** **pertinence, impact et leçons apprises (Gergely Sipos, Fondation EGI)** : le programme Early Adopter (fournisseurs) et les centres de compétences (utilisateurs et fournisseurs soutenus) ont travaillé en étroite collaboration en tant qu'instruments d'engagement avec les communautés de recherche, et étaient similaires à bien des égards. Les centres de compétences étaient des mini-projets sélectionnés sur la base des communautés scientifiques qui ont exprimé leur intérêt à collaborer, tandis que les programmes Early Adopter ont été mis en place par le biais de 2 appels ouverts au cours du projet. Les appels ouverts sont des instruments extrêmement utiles pour s'engager auprès des communautés scientifiques européennes. Le plus grand avantage pour les communautés a été le réseau de conseil pour trouver la combinaison la plus appropriée de services/ressources et ensuite les aider à les intégrer dans leurs propres flux de travail.

\- **Soutenir l'engagement disciplinaire avec l'EOSC (Timea Biro, RDA4EOSC)** : Une étude qui cartographie l'état de connaissance et de maturité des domaines/communautés de recherche disciplinaires, identifie les cas sous-représentés et fournit des recommandations pour un engagement futur. La plupart des communautés évaluées se trouvent soit conscientes mais pas (entièrement) prêtes, soit prêtes mais pas conscientes de l'EOSC ni des avantages de s'engager avec la communauté interdisciplinaire plus large et de se joindre à l'effort.

\- **EOSC DIH : Bridging industry and EOSC (Sy Holsinger, EGI Foundation)** -- l'EOSC Digital Innovation Hub (DIH) soutient l'innovation au sein des entreprises privées en utilisant les services, les données et l'expertise de l'EOSC. Il y a eu 18 pilotes d'entreprise pendant le projet EOSC-Hub. Ils seront poursuivis durant le projet EOSC Future. Il s'associe à des projets d'infrastructure clés, des start-ups, des spin-offs d'universités, des projets régionaux de l'EOSC, des associations de PME, des espaces de données et des fournisseurs de données. Le DIH a mis en place des sites web pérennes pour informer sur les projets pilotes, les partenariats établis et les opportunités de financement supplémentaires autour de la communauté.

Priorités horizontales

\- De nombreuses communautés scientifiques ne sont pas prêtes pour l'arrivée de l'EOSC et ne sont pas pleinement conscientes des avantages de la science multidisciplinaire.

\- Un changement de culture dans la façon dont les chercheurs abordent la recherche scientifique est nécessaire afin de réduire la perception du risque associé au partage des données. Les récompenses et les incitations sont essentielles à cet égard.

\- Les appels ouverts peuvent devenir un instrument essentiel pour s'engager auprès des communautés scientifiques européennes. Ils devraient être soutenus par des réseaux de soutien et des conseils coordonnés. Le DIH en est un exemple pour la communauté des PME.

\- Les réseaux locaux de praticiens de la science ouverte peuvent aider à cultiver ce changement de culture. Une approche locale peut favoriser une plus grande confiance dans l'utilisation des principes de données FAIR.

**Sous-groupe de discussion : tables rondes -- indicateurs de maturité & données et services**

Deux tables rondes se sont tenues dans cette session de sous-groupe, la première sur les indicateurs de maturité et la seconde sur les données et services.

Une approche du tableau de bord concernant les indicateurs de maturité de l'EOSC : co-concevoir la voie à suivre (INFRAEOSC-5 Landscaping Task Force)

**Présidente : Federica Tanlongo**

GARR & EOSC-Pillar

-   À partir d'une proposition initiale, un ensemble d'indicateurs et de méthodologies a été conçu lors d'un atelier de validation, puis révisé lors de consultations avec les principales parties prenantes.

-   18 indicateurs dans 5 macro-domaines clés ont été identifiés et analysés via un tableau de bord. Les pays analysés étaient l'Italie, le Portugal, la Serbie et la Slovénie.

-   Les principales fonctionnalités des tableaux de bord sont :

```{=html}
<!-- -->
```
-   récolter automatiquement des informations à partir de sources de données ouvertes et fiables

-   la possibilité de télécharger des données manuellement

-   la transparence

-   une vue d'ensemble de l'Europe, assortie de statistiques spécifiques à chaque pays

-   une page dédiée à chaque pays avec des informations quantitatives et qualitatives

NEANIAS, Cos4Cloud, TRIPLE, INODE : 4 projets de l'EOSC s'associent pour façonner le monde des données et des services en Europe (INFRAEOSC-2)

**Présidente : Eleni Petra**

Athena RC & NEANIAS

-   NEANIAS répond aux besoins spécifiques de la communauté pour les secteurs de la recherche sous-marine, atmosphérique et spatiale afin d'amener la communauté à la science ouverte, de favoriser de nouvelles opportunités commerciales et de renforcer l'EOSC.

-   Cos4Cloud relie la science citoyenne à l'EOSC en mettant en relation différentes infrastructures de science citoyenne, connues sous le nom d'observatoires citoyens (CO), par le biais de services intégratifs conçus conjointement. Les CO peuvent fournir des données complémentaires pour la recherche sur un large éventail de sujets, tels que la biodiversité ou la qualité de l'environnement.

-   TRIPLE fournit une solution de découverte multilingue pour les sciences humaines et sociales par le biais d'un point d'accès unique qui permet aux utilisateurs d'explorer, de trouver, d'accéder et de réutiliser des publications et des données, des projets et des profils de chercheurs à l'échelle européenne.

-   INODE aide les utilisateurs des domaines de l'astrophysique, de la recherche sur les biomarqueurs du cancer et de l'élaboration des politiques de recherche et d'innovation à relier et à exploiter de multiples jeux de données, à accéder aux données et à les rechercher en utilisant le langage naturel, à exploiter des exemples et des analyses, ainsi qu'à obtenir des conseils pour comprendre les données et formuler des requêtes, explorer les données et découvrir de nouvelles perspectives grâce à des visualisations.

**Prochaines étapes**

\- Engagement et adoption des chercheurs :

-   Clarifier les questions relatives aux ressources disponibles et à la portée de la task force sur l'engagement et l'adoption des chercheurs

-   Analyser et comprendre comment les chercheurs sont engagés aux niveaux national et institutionnel, quelles mesures d'engagement ont déjà été prises et quels en sont les effets et les résultats.

-   Établir un plan/une stratégie de communication

-   Établir un plan d'activité en tenant compte des ressources disponibles

\- Politique et mise en œuvre des PID :

-   Identification des projets de recherche européens passés et actuels traitant des PID

-   La coopération à un projet de recherche aboutit à évaluer et sélectionner les résultats compatibles avec l'EOSC

-   Interview des gestionnaires des entrepôts de données européens pour identifier les meilleures pratiques en matière de PID et produire une analyse SWOT

\- Contrôle de la conformité aux règles de participation :

-   Réunions régulières

-   Ateliers avec les propriétaires et fournisseurs de catalogues

-   Ateliers avec les fournisseurs et les communautés de recherche ainsi que les projets

```{=html}
<!-- -->
```
-   Indicateurs de maturité de l'EOSC :

```{=html}
<!-- -->
```
-   De nombreux indicateurs identifiés ne sont pas réalisés sous la forme de jeux de données prêts à l'emploi

-   Certains jeux de données sont disponibles au niveau européen, mais ils ne sont pas exactement liés aux indicateurs définis, les proxys qui les utilisent doivent être définis

-   Parallèlement, une analyse de la disponibilité des données au niveau national est en cours

-   L'intégration de certains jeux de données dans le tableau de bord ainsi que la publication d'un rapport final seront terminées à l'été 2021

**Vendredi 18 juin 2021**

**Priorités de mise en œuvre**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

11:15-13:00 Qualité des données et des métadonnées

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

**Présidente : Sarah Jones**

GÉANT et directrice, association EOSC

**Introduction**

La session sur la qualité des données et des métadonnées, présidée par Sarah Jones (GÉANT et directrice, association EOSC), s'est concentrée sur l'interopérabilité sémantique pour garantir la découverte et la réutilisation des données, ainsi que sur les aspects de la qualité des données et des systèmes d'indicateurs pour promouvoir des ressources de grande qualité dans l'EOSC. La réunion a commencé par une brève présentation de la charte de la task force (TF) sur l'interopérabilité sémantique ainsi que de la charte de la TF sur les systèmes d'indicateurs FAIR et la qualité des données. Des contributions de la communauté sur ces deux sujets ont suivi. Des discussions sur les solutions potentielles ont clôturé la session.

**Principaux points à retenir**

\- La TF sur l'interopérabilité sémantique a déjà identifié plusieurs problèmes et besoins qui devront être traités afin de faire progresser les solutions d'interopérabilité. Les problèmes que la TF devra traiter incluent le manque et/ou la surabondance de définitions explicites, de sémantique commune/ontologies générales, d'entrepôts de référence, de systèmes de métadonnées communs à toutes les communautés, et de modèles de métadonnées. Des approches de principe pour les systèmes d'ontologie et de métadonnées, l'harmonisation entre les disciplines, l'harmonisation des données du même type ainsi que l'accès fédéré aux entrepôts de données de recherche existants sont par conséquent nécessaires.

\- Dans le contexte de l'EOSC, les systèmes d'indicateurs FAIR ont déjà été considérés dans une certaine mesure, alors que la qualité des données n'a pas encore été suffisamment explorée. La TF « Systèmes d'indicateurs FAIR et qualité des données » vise donc à superviser la mise en œuvre des indicateurs FAIR en les testant avec les communautés de recherche et en considérant les aspects plus généraux de la qualité des données.

\- Parmi les communautés qui ont contribué à la session sur l'interopérabilité sémantique, citons l'Union internationale de cristallographie (IUCr), qui tente d'harmoniser les Crystallographic Information Files et les Crystallographic Information Frameworks (CIF), ExPaNDS, qui présente des ontologies pour la science des photons et des neutrons, le Forschungszentrum Jülich, qui introduit la base de données TOAR, eLTER, qui applique le RDA I-Adopt Framework, et Reliance, qui présente des modèles de métadonnées et des objets de recherche pour les cubes de données d'observation de la Terre.

\- La session sur les systèmes d'indicateurs FAIR et la qualité des données a bénéficié d'une contribution de FAIRsFAIR qui a présenté FAIR-Aware, un outil utilisé pour évaluer la sensibilisation des personnes aux principes FAIR. Deux autres intervenants ont donné un aperçu de l'état actuel de la FAIRitude dans l'analyse des données de cryo-microscopie électronique, et un aperçu des informations de provenance standardisées pour les spécimens biologiques respectivement. Ce dernier point est confronté à des défis tels que le fait que les spécimens et les données sont souvent déconnectés, ou que le retraçage des données analysées jusqu'aux données brutes ou des générations de données jusqu'aux spécimens est souvent impossible.

\- Enfin, des solutions potentielles ont été présentées. Parmi elles, le rôle de la DDI-CDI dans l'EOSC : utilisations et applications possibles, la plateforme Helmholtz Metadata Collaboration (HMC) ainsi que le service de terminologie TIB comme service de base pour les infrastructures de données de recherche.

**Prochaines étapes**

\- Les présentations ont donné lieu à des discussions animées dans les tchats. Des liens ainsi que des demandes de contact ont été échangés afin d'approfondir des discussions spécifiques et d'échanger des connaissances ainsi que des expériences. Cet échange organique de connaissances doit être encouragé et poursuivi.

\- La DDI-CDI (Cross-Domain Integration) a été développé par les membres de l'initiative de documentation des données (DDI) au service des projets de recherche SBE utilisant des données d'autres domaines. Trois caractéristiques essentielles pour faciliter la combinaison des données et l'actionnabilité des machines ont été retenues : la structure (décrivant les rôles joués par les données dans diverses structures de données), la provenance (décrivant la provenance et le traitement des données) et la description des variables (concepts, variables, classifications, codage, etc.). Sur la base de leurs expériences, exemples et cas d'utilisation, les recommandations et activités suivantes sont suggérées : renforcer le soutien à l'intégration des données, automatiser la saisie des métadonnées, développer des passerelles vers les normes du domaine, relier la DDI-CDI à d'autres normes de métadonnées au sein de l'infrastructure de métadonnées de l'EOSC, établir des lignes directrices pour la fourniture de métadonnées, soutenir les solutions technologiquement neutres, s'aligner sur les initiatives internationales de métadonnées et sur les technologies et plateformes de mise en œuvre pertinentes.

\- La plateforme HMC aide les chercheurs à décrire (automatiquement) leurs données au moyen d'une description appropriée, conforme aux normes, avec des métadonnées. Elle leur permet également de réutiliser les données de recherche Helmholtz pour des méthodes avancées de traitement et d'analyse des données. Pour ce faire, elle réunit les Metadata Hubs (expertise communautaire, formation et technologies), FAIR Data Commons (services techniques et FAIRisation), les projets HMC (communauté et cas d'utilisation) et le HMC Office (gestion et contrôle).

\- TIB espère tirer parti de leurs services - notamment le support technique, la formation et la consultation, les services de vocabulaire et la conservation, ainsi que les questions juridiques et les licences - dans l'EOSC. Les prochaines étapes comprennent la mise en œuvre du service de terminologie (TS) dans d'autres projets TIB, des contrôles de qualité et des pipelines pour l'incorporation d'ontologies de tiers, l'extension des API pour la récupération de données ontologiques lisibles par machine, le développement d'ontologies spécifiques à un domaine pour les projets NFDI, des frontends spécifiques à un domaine pour les projets NFDI, la mise en œuvre de modules de conservation et d'édition dans le TS, ainsi que l'établissement d'un service de notification des ontologies publiées, des changements et des mises à jour.

Vendredi 18 juin 2021

Priorités de mise en œuvre

13:00-14 :00 Déjeuner et EOSC-Clinic : session de questions ouvertes

**Présidents : Bob Jones**

CERN & Directeur, association EOSC

**Ignacio Blanquer**

UPV & Directeur, association EOSC

**Introduction**

Au cours de cette session, les participants ont eu l'occasion de discuter des points de vue et des attitudes des fournisseurs commerciaux vis-à-vis de l'initiative EOSC. Ils ont également abordé le plan de diffusion et d'engagement de l'association EOSC sur la scène nationale et internationale.

**Principaux points à retenir**

\- Les groupes consultatifs (AG) et les task forces (TF) devraient travailler ensemble pour relever les défis techniques afin de comprendre les besoins respectifs des TF, notamment en termes de niveau d'intégration des données.

\- Les champions sont essentiels pour approcher la communauté et ils peuvent constituer une passerelle entre les domaines de la recherche et les domaines techniques. Les scientifiques de renom sont de bons catalyseurs pour leurs disciplines respectives et ils ont de bonnes relations.

\- L'association EOSC doit définir le futur plan/stratégie de diffusion, et présenter les principales conclusions des symposiums EOSC et les exemples de réussite de ses membres.

\- L'engagement et l'enthousiasme pour l'EOSC se manifestent dans de nombreux domaines.

\- L'EOSC est un leader mondial avec son concept multi-pays et multidisciplinaire.

**Prochaines étapes**

-   Les AG et les TF devraient travailler ensemble.

-   L'association EOSC a besoin de définir le futur plan/stratégie de diffusion.

-   Il est important de partager les visions, les outils et les services de l'EOSC avec les petits pays, en les encourageant à faire partie de la communauté de l'EOSC.

Vendredi 18 juin 2021

Priorités de mise en œuvre

14:00-15:45 : le symposium de l'EOSC et au-delà

**Président : Nicholas Ferguson**

Trust-IT Services & EOSCsecretariat.eu & EOSC Future

**Introduction**

Le Symposium de l'EOSC et au-delà, présidé par Nicholas Ferguson (Trust-IT Services & EOSCsecretariat.eu), était la dernière session du Symposium de l'EOSC 2021. Elle a jeté un regard rétrospectif sur les réalisations du projet EOSCsecretariat.eu qui a soutenu la gouvernance de l'EOSC 2019-2021 et a également présenté les plans du projet EOSC Future, récemment financé, qui vise à exposer une plateforme EOSC opérationnelle. La session a été clôturée par le président de l'association EOSC, Karel Luyben, qui a remercié tous ceux qui ont contribué à faire de l'European Open Science Cloud ce qu'il est aujourd'hui et qui a regardé l'avenir avec un esprit positif en faisant remarquer que nous, l'ensemble de la communauté de l'EOSC, sommes le véritable écosystème de l'EOSC.

Visionner la session complète

**Principaux points à retenir**

\- EOSCsecretariat.eu a ouvert la voie à la création de l'association EOSC, a agi comme un agrégateur pour la communauté de l'EOSC croissante en collaborant avec les ESFRI, la communauté HPC, les projets Call5b et des initiatives telles que GAIA-X, tout en soutenant la co-création de l'EOSC grâce à un budget dédié à la co-création.

\- Le financement accéléré pour la Covid-19 de EOSCsecretariat.eu a démontré très concrètement la valeur ajoutée de la science ouverte.

\- L'objectif de l'EOSC Future est de mettre en œuvre un EOSC-Core opérationnel, un système de systèmes, qui agit comme une « couche de colle » combinant les ressources à travers les infrastructures, et d'alimenter l'EOSC-Exchange.

\- L'EOSC est plus qu'un portail. Il ne s'agit pas seulement de machines, mais aussi de personnes. « Nous » sommes l'écosystème de l'EOSC.

\- L'association EOSC collaborera avec la communauté et s'efforcera de mieux relier les diverses contributions, mais en même temps, l'association doit trouver un équilibre entre aller de l'avant et consulter la communauté aussi largement que possible.

**Prochaines étapes**

\- L'EOSC devrait s'appuyer sur ce qui a déjà été fait en termes d'engagement et poursuivre la coopération avec l'ensemble de la communauté de l'EOSC. Cependant, une plus grande attention devrait être portée à la communauté des chercheurs.

\- Le marketing de l'EOSC est nécessaire. L'association EOSC devra communiquer activement avec les différents groupes en mettant en évidence la valeur ajoutée de l'EOSC pour chacun d'entre eux.

\- On ne peut pas faire grand-chose tout seul. L'association EOSC aura un rôle actif et collaboratif dans la phase de mise en œuvre de l'EOSC.

eoscsecretariat.eu eosc.eu

\@EoscSecretariat \@eoscassociation

/company/eoscsecretariat

eosc.eu \@eoscssociation

EOSCSecretariat.eu a reçu un financement du programme Horizon de l'Union européenne, appel H2020-INFRAEOSC-05-2018-2019, accord de subvention n° 831644.

\[1\] Toutes ces informations et bien d'autres encore sont disponibles sur le site https://ec.europa.eu/info/ news/horizon-europe-info-days-eosc-symposium-presenting-infraeosc- destination-2021-and-2022-2021-jun-15_en

\[2\] <https://rolect.ni4os.eu/rolect/auth/login>
