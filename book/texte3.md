# Six recommandations pour la mise en œuvre des pratiques FAIR

<https://op.europa.eu/en/publication-detail/-/publication/4630fa57-1348-11eb-9a54-01aa75ed71a1/language-en/format-PDF/source-166584930>

RÉSUMÉ

Ce rapport analyse l\'état des pratiques FAIR au sein de diverses communautés de recherche et les politiques liées à FAIR dans différents pays et propose six recommandations sur la façon dont ces pratiques doivent être mises en œuvre. Ces recommandations s\'adressent principalement aux organes de décision de l\'European Open Science Cloud (EOSC), ainsi qu\'aux financeurs de la recherche :

1 : Financer la sensibilisation, la formation, l\'éducation et le soutien spécifique aux communautés.

2 : Financer l'élaboration, l\'adoption et la mise à jour des standards, outils et infrastructures communautaires.

3 : Encourager le développement de la gouvernance communautaire.

4 : Traduire les directives FAIR pour d\'autres objets numériques.

5 : Récompenser et reconnaître les améliorations dans les pratiques FAIR.

6 : Développer et assurer un suivi des politiques publiques adéquates pour les données et les objets de recherche FAIR.

Afin d\'assurer des avantages élargis pour l\'EOSC, des améliorations des pratiques FAIR sont nécessaires. Nous pensons que le moment choisi pour ce rapport, qui coïncide avec le lancement complet de l\'EOSC, pourrait aider cette infrastructure, les bailleurs de fonds de la recherche et les décideurs politiques à prendre des décisions stratégiques cruciales sur les investissements nécessaires pour mettre les principes FAIR en pratique.
