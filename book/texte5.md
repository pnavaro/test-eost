# Compétences numériques pour FAIR et la science ouverte

<https://op.europa.eu/en/publication-detail/-/publication/af7f7807-6ce1-11eb-aeb5-01aa75ed71a1/language-en/format-PDF/source-190694287>

RÉSUMÉ

Les compétences numériques pour FAIR[^1] et la science ouverte sont une pierre angulaire du fonctionnement et de l\'avenir de l\'European Open Science Cloud (EOSC). Un réseau EOSC de professionnels qualifiés est essentiel pour engendrer un changement de culture favorable au partage des résultats de la recherche, et pour permettre aux personnes concernées et aux institutions de développer et de maintenir les compétences, les aptitudes et les capacités nécessaires dans l'EOSC.

Le groupe de travail (WG) sur les compétences et la formation de l\'EOSC a été formé en 2020 afin d\'identifier un cadre pour le développement des compétences et des capacités de l\'EOSC. Le groupe de travail s\'est concentré sur quatre domaines prioritaires qui constituent les principales sections de ce rapport :

**Développer la prochaine génération de professionnels FAIR et de la science ouverte :** présente un cadre de tous les acteurs (rôles) de l\'écosystème de l\'EOSC pour lesquels les compétences et la formation sont pertinentes.

**Collaborer pour améliorer les compétences numériques pour FAIR et la science ouverte en Europe :** examine les approches organisationnelles pour mettre en œuvre des activités et des programmes de formation, moyennant le concept de centres de compétences.

**Construire un centre de connaissances fiable et durable de ressources d\'apprentissage et de formation avec outils associés :** fournit des idées pour un catalogue de formation fédéré de l\'EOSC intégré à une infrastructure de formation durable en soutien des acteurs de l\'EOSC.

**Influencer une politique nationale de compétences en science ouverte par un soutien aux responsables de stratégies** : analyse la définition des compétences numériques requises dans l\'EOSC dans le cadre plus large de la stratégie européenne sur les compétences, en vue de proposer des recommandations aux États membres et aux pays associés sur la manière de soutenir l\'EOSC dans leurs politiques et stratégies de compétences nationales.

L\'analyse des lacunes réalisée par le WG démontre qu\'un travail important est encore nécessaire de la part d\'une grande variété de parties prenantes, non seulement pour concrétiser la vision de l\'EOSC, mais aussi pour accentuer l\'impact de la recherche au niveau international. Alors que ce rapport identifie les prochaines étapes pour surmonter les obstacles et tirer parti des opportunités pour un développement des compétences et de la formation aussi accru que possible, une attention particulière est nécessaire pour continuer à faire progresser ce domaine à l\'avenir. Les recommandations du WG sur les prochaines étapes sont les suivantes :

**Principales recommandations**

1\. Utiliser le schéma des acteurs de l\'écosystème de l\'EOSC (Figure 3) dans le développement d\'initiatives, de compétences, de formations, de mécanismes de récompense et de reconnaissance, ainsi que de parcours professionnels nécessaires à un développement et une généralisation de FAIR et de la science ouverte accrus.

2\. Coordonner et harmoniser les programmes d\'enseignement et les cursus de formation en créant un consensus autour d'un programme d\'enseignement supérieur européen de base pour transmettre les compétences FAIR et de science ouverte au niveau universitaire.

3\. Encourager et soutenir l\'approche des centres de compétences comme schéma visant à accroître l\'offre coordonnée de formations harmonisées pour soutenir les pratiques FAIR et la science ouverte.

4\. Faciliter une plus forte intégration des cours sur les pratiques FAIR et la science ouverte dans les qualifications universitaires.

5\. Construire un catalogue d\'apprentissage et de formation en utilisant les préconisations de développement recommandées par ce WG pour accentuer le plus possible l\'interopérabilité.

6\. Faire figurer les ressources d\'apprentissage et de formation à l'intérieur du cadre d\'interopérabilité de l'EOSC (EIF).

7\. Développer un programme de compétences et de formation de l\'EOSC pour dirigeants pour :

Accroître la coordination des politiques, programmes et réseaux européens et nationaux soutenant les éléments de compétences de FAIR et de la science ouverte. Développer et promouvoir un programme d\'ambassadeurs de compétences et de formation de l\'EOSC pour conseiller les décideurs nationaux.

Plaider pour l\'inclusion des compétences et de la formation aux principes FAIR et à la science ouverte dans les principaux instruments de financement européens et nationaux.

Développer et promouvoir un programme d\'ambassadeurs de compétences et de formation de l\'EOSC pour conseiller les décideurs nationaux.

Plaider pour l'intégration des compétences et de la formation à FAIR et à la science ouverte dans les principaux instruments de financement européens et nationaux.

[^1]: FAIR signifie Trouvable, Accessible, Interopérable et Réutilisable (Wilkinson et al. 2016).
