# Solutions pour un EOSC durable

<https://op.europa.eu/en/publication-detail/-/publication/581d82a4-2ed6-11eb-b27b-01aa75ed71a1/language-en/format-PDF/source-175468053>

RÉSUMÉ

Ce document explore les moyens possibles d'assurer la continuité de l'European Open Science Cloud au-delà de sa phase initiale qui se termine à la fin de 2020. Ce document indépendant s\'appuie sur les versions précédentes (strawman et tinman) et sur les retours de la part du Bureau exécutif et du Comité de gouvernance de l\'EOSC, ainsi que de la Commission européenne (CE) et de la communauté des parties prenantes sur chaque version. Il prend également en compte les progrès accomplis dans la réalisation des objectifs de l\'EOSC ainsi que les résultats des études commandées. Il examine le modèle de financement, le dispositif juridique, la structure de gouvernance dans le cadre du partenariat européen prévu avec la CE ainsi que le contexte réglementaire et d'orientation de l\'EOSC. Il recommande de commencer par une première itération pour établir un socle minimum de l'EOSC (MVE) répondant aux besoins des chercheurs financés par des fonds publics et exploitant des données librement disponibles. Les itérations suivantes prolongent l'EOSC pour aborder l\'utilisation au-delà des données FAIR en libre accès et impliquer une base d\'utilisateurs plus large incluant le secteur public et le secteur privé.
