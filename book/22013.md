# Recommandations

1 Résumé exécutif

Ce livrable rassemble les réactions du consortium du projet à l\'actuel Agenda stratégique de recherche et d\'innovation (SRIA) pour l\'EOSC, en identifiant les aspects déjà abordés par le projet EOSC Future et ceux pour lesquels des activités supplémentaires sont nécessaires. Il a été programmé pour coïncider avec les processus de consultation de la Commission européenne visant à obtenir des contributions pour la feuille de route pluriannuelle de 2023-2024 afin d\'aider à définir le programme de travail à venir.

L\'introduction présente le contexte de l\'élaboration initiale du SRIA et le calendrier des contributions. La section 3 décrit l\'état actuel des choses en examinant les priorités existantes et la manière dont elles sont traitées dans le projet EOSC Future. Les sujets préliminaires proposés par l\'Association EOSC sont également décrits dans cette section, puisqu\'ils ont été pris en compte dans le feedback du consortium. Le chapitre le plus important est la section 4 qui présente les recommandations issues du projet EOSC Future.

Les onze priorités notées dans le projet portent sur :

\- élargir l\'EOSC par des alignements stratégiques avec les infrastructures de recherche (IR), les espaces de données et les organisations de recherche ;

\- fournir de meilleures connexions entre les services et les modèles d'affectation des ressources de l'EOSC afin de permettre un accès plus transparent entre eux ;

\- garantir des dépôts numériques fiables qui favorisent des données de qualité et la conservation à long terme ;

\- répondre au soutien humain et aux compétences nécessaires pour faciliter l\'adoption de l\'EOSC ;

\- mettre en place des outils permettant de suivre l\'activité de recherche afin de l\'encourager.

Les recommandations ont été partagées avec la DG-RTD et la DG-CNECT de la Commission début novembre afin de contribuer à leurs réflexions. Une première version du programme de travail sera disponible au début de l\'année 2022.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\[...\]

\_\_\_\_\_\_\_\_\_\_\_\_\_\_

4 Recommandations d'EOSC Future

Vous trouverez ci-dessous une liste de domaines prioritaires que le consortium EOSC Future a proposé d\'examiner dans le cadre du programme de travail 2023-2024. Ces domaines peuvent être classés comme suit :

\- élargir l\'EOSC par des alignements stratégiques avec les infrastructures de recherche, les espaces de données et les organisations de recherche ;

\- fournir de meilleures connexions entre les services et les modèles d'affectation des ressources de l'EOSC afin de permettre un accès plus transparent entre eux ;

\- garantir des dépôts numériques fiables qui favorisent des données de qualité et la conservation à long terme ; répondre au soutien humain et aux compétences nécessaires pour faciliter l\'adoption de l\'EOSC ; et

\- mettre en place des outils permettant de suivre l\'activité de recherche afin de l\'encourager.

Les principaux domaines thématiques et leurs interrelations sont illustrés dans le diagramme ci-dessous.

Figure 4.2 : Thèmes prioritaires d'EOSC Future

Contenu de la figure 4.2 :

Élargissement via les partenaires stratégiques et les organismes nationaux

Soutien humain, formation et compétences \| Suivi via des profils et SKG

Conservation / Qualité et confiance

Entrepôts de données / Lacs de données

Calcul, stockage et logiciel

Modèles flexibles pour allocation de ressources

4.1 Élargissement de la base d\'utilisateurs de l'EOSC

L\'EOSC doit étoffer sa base d\'utilisateurs et établir des liens avec d\'autres initiatives connexes pour regrouper les données de différents secteurs nécessaires pour relever les défis sociétaux. En 2023-2024, l\'accent devrait être mis sur l\'intégration de l\'EOSC dans les principales opérations des infrastructures de recherche, des missions et des partenariats. En collaborant avec les espaces de données et d\'autres initiatives stratégiques, l\'EOSC peut contribuer à relever les défis sociétaux à travers ses actions.

Les travaux actuels visant à fédérer les infrastructures numériques et les infrastructures de recherche dans le cadre d'EOSC Future doivent être poursuivis afin de se consacrer à un domaine ou à un secteur particulier pour faire face aux difficultés mondiales. Les infrastructures de recherche distribuées et les e-infrastructures ont le potentiel de contribuer au développement socio-économique local et régional. Il est possible de développer des stratégies de coopération ESFRI / EOSC pour la création de pôles de connaissances locaux / régionaux en mettant en adéquation les installations de pointe, les capacités de données et la montée en compétences du personnel hautement qualifié.

Les missions européennes et les projets de partenariat peuvent servir de cas d'usage et de validation de principe pour établir les meilleures pratiques à adopter dans le contexte de l\'intégration de jeux de données hétérogènes pour une IA avancée qui est en phase avec les défis environnementaux, sociaux et économiques mondiaux, démontrant la valeur de l'EOSC et des données FAIR.

Il convient également de soutenir l\'élaboration de solutions pour l\'accès international aux données publiques dans des environnements de dépôts fiables et de favoriser les accords avec l\'industrie et les organisations à grand volume de données dans le cadre des efforts globaux d\'élargissement. Il existe actuellement des possibilités d\'activités conjointes entre les nœuds d'infrastructures de recherche africaines et européennes sur le thème du changement climatique dans l\'appel INFRA-2021-DEV-01-02, et des collaborations infrastructurelles similaires pourraient être poursuivies dans le prochain programme de travail pour soutenir l\'élargissement de l\'EOSC à d\'autres partenaires internationaux.

4.2 Engagement des institutions et organismes nationaux

A l\'heure actuelle, l\'EOSC est toujours dominé par les infrastructures de recherche à grande échelle et les services horizontaux. Avec la formation de l\'Association EOSC et le nombre important de membres issus d\'organisations de recherche, une opportunité se présente de mieux relier les infrastructures de soutien locales à l\'EOSC.

Il convient d\'en faire plus pour établir des liens au niveau national avec les universités, les communautés de recherche et les chercheurs individuels. Tirer profit des connexions avec les financeurs nationaux de la recherche, les fournisseurs de services tels que les NREN, NGI et NOAD, ainsi que les organismes représentatifs tels que l\'Alliance des universités européennes permettrait d\'impliquer la longue traîne de la science avec le soutien des organismes de recherche. La récente étude sur les structures nationales de l\'EOSC fournit de nombreux points de départ, tout comme les organisations mandatées parmi les membres de l\'Association EOSC et les forums tels que le CoNOSC[^1].

En harmonisant l\'infrastructure de soutien institutionnel avec l\'EOSC, il sera possible de mieux bénéficier des compétences du personnel de soutien institutionnel, comme les gestionnaires de données, afin de renforcer la capacité à soutenir les principes FAIR et la science ouverte, comme indiqué dans la section Infrastructure humaine. Des modèles tels que le réseau néerlandais des centres de compétences numériques[^2] pourraient être explorés afin d\'établir des liens plus étroits entre le soutien dans les organismes de recherche et l\'EOSC. L\'engagement au niveau régional pour soutenir les activités de coordination aidera à résoudre les problèmes pratiques d'embarquement de l'EOSC.

4.3 Modèles flexibles pour l\'allocation des ressources

L'EOSC est présenté comme un environnement dans lequel les chercheurs peuvent accéder aux données et aux services associés, mais la manière dont l\'accès à ces services sera financé et alloué n\'est pas claire. Il n\'existe pas de modèle permettant aux chercheurs individuels ou aux petites équipes qui souhaitent examiner les données accessibles via l'EOSC de disposer de l\'informatique nécessaire. Il existe des processus d\'exigences claires pour que les chercheurs puissent demander l\'accès aux ressources informatiques par le biais des grandes infrastructures de recherche, mais il n\'y a pas encore de processus définis pour que d\'autres puissent accéder de manière transparente à la capacité informatique pour visualiser et traiter les données dans le contexte de l\'EOSC. C\'est surtout un problème pour les petits jeux de données, la science citoyenne ou les actions exploratoires, car ceux qui traitent des téraoctets de données suivront les processus existants.

L\'accès aux services est toujours géré par des fournisseurs individuels et ne permettra pas à une IR ou à un centre de calcul national de financer les ressources informatiques nécessaires au traitement des données d\'autres utilisateurs ou d\'utilisateurs pour lesquels le soutien ne fait pas partie de leurs attributions. Si les utilisateurs finaux ne peuvent pas accéder facilement aux ressources au sein de l\'EOSC, la pratique consistant à transférer les données sur l'ordinateur fixe/portable prévaudra et les avantages de fournir un environnement où les données peuvent non seulement être trouvées mais également consultées et traitées ne seront pas réalisés.

Dans le cadre des projets INFRAEOSC-07, des modèles d'affectation des ressources sont testés pour fournir des services gratuits au point d\'utilisation, mais cela ne peut être proposé que pour les services inclus dans ces projets. Les autres fournisseurs ne sont pas éligibles pour recevoir un financement, il ne s\'agit donc pas d\'un modèle ouvert pour les services et les ressources mis à disposition dans le cadre d\'EOSC-Exchange. Le modèle d\'approvisionnement exploré pour EOSC-Core est également adapté aux fournisseurs de services commerciaux, alors que beaucoup de ceux qui opèrent actuellement dans l'EOSC viennent du domaine scientifique et ne sont pas autorisés à participer à des achats commerciaux.

Bien qu\'une activité supplémentaire sur les modèles d'affectation de ressources soit prévue dans les projets à venir, un effort continu est nécessaire pour les mettre pleinement en œuvre. Les fournisseurs de services doivent également avoir accès à une expertise pour les aider à comprendre les modèles commerciaux et les questions juridiques liées à la fourniture de services transfrontaliers dans le contexte de l\'EOSC. Les modèles de financement et d\'allocation des ressources développés pour l\'EOSC doivent être soutenus par la CE et être alignés sur les mécanismes de subvention des États membres.

4.4 De meilleures connexions entre les services de stockage, de calcul et de logiciels

Les chercheurs travaillent dans divers environnements pour mener à bien leurs travaux et ceux-ci doivent être réunis de manière efficace au sein de l\'EOSC, avec des liens très nets entre les installations et les institutions pour permettre aux chercheurs de transférer les données à analyser en différents endroits. De nombreux chercheurs peuvent générer des téraoctets de données dans des installations de recherche nationales, puis ne pas avoir accès à la puissance de calcul et aux outils nécessaires pour les traiter dans leur institution d\'origine. L\'écosystème EOSC devrait être une interface entre les infrastructures de recherche (IR), les e-infrastructures, le calcul à haute performance (HPC), une grande variété d\'outils et de fournisseurs de services, et les institutions où les chercheurs sont basés et mènent leurs travaux. Les données pourront ainsi être facilement stockées, transférées, combinées, traitées, publiées, conservées et partagées, et les chercheurs pourront trouver les ressources nécessaires pour exécuter ces actions, indépendamment de leur provenance.

Un mécanisme d\'authentification interopérable est nécessaire pour que les scientifiques puissent accéder aux services EuroHPC parallèlement à l\'offre EOSC. EOSC Future a consacré des ressources à l\'étude des liens entre EuroHPC et l'EOSC. À cette fin, plusieurs pilotes ont déjà commencé à étudier les synergies dans de multiples domaines scientifiques qui pourraient être exploitées dans les programmes futurs.

L\'Europe dispose d\'importantes ressources de données et de calcul qui sont d\'une utilité vitale pour les communautés de recherche européennes, mais les infrastructures actuelles ont besoin de nouveaux investissements significatifs pour se développer et faire face au déluge de données attendu au cours de la prochaine décennie. Les États membres devraient accroître leurs investissements nationaux pour assurer le leadership en matière de technologie et de recherche et, par conséquent, encourager l\'accès transnational aux installations de traitement des données en nuage et à haut débit via l\'EOSC, qui ne seraient autrement accessibles qu\'aux communautés nationales d\'utilisateurs. Comme le volume, la variété, la vélocité et les volumes de données augmentent, l\'EOSC doit distribuer et décentraliser de manière sécurisée le traitement de données de recherche de haute qualité dans un continuum informatique offrant une gestion de l\'hétérogénéité du matériel.

L\'infrastructure transfrontalière fédérée de l\'UE en nuage et en périphérie de réseau, dotée d\'une capacité informatique supplémentaire, doit étendre la capacité nationale, offrir un accès transnational et être totalement intégrée aux clouds de recherche nationaux, en les développant grâce à une capacité informatique supplémentaire achetée au niveau national et en offrant un accès intégré aux technologies HTC, HPC, en nuage et en périphérie de réseau.

La plateforme de calcul de l\'EOSC doit s\'intégrer aux lacs de données fédérés et aux dépôts de données de l\'EOSC afin de démocratiser l\'analyse des ressources de données essentielles à grande échelle et de préserver la souveraineté des données de recherche européennes. Les données provenant de multiples sources distribuées doivent être rassemblées sur une plateforme de traitement et d\'analyse adaptée, qui prenne en charge les plateformes Digital Twin, les observatoires virtuels et les environnements scientifiques virtuels de recherche.

Une telle plateforme devra se développer avec la contribution des pays de l\'UE13 et de l\'élargissement de l\'UE. La plateforme doit également s\'aligner sur les espaces de données, avec des solutions intégrées pour la gestion des lacs de données, l\'orchestration, l\'AAI fédérée, et un portefeuille de logiciels scientifiques, de modèles et de capacités de moteurs Digital Twin ainsi que de systèmes EuroHPC.

4.5 Lacs de données et dépôts de données fédérés de l\'EOSC

L\'objectif du projet EOSC Future est de réaliser une plateforme opérationnelle avec un environnement d\'exécution intégré composé de données, de services, de produits de recherche ouverts et d\'infrastructures. Afin d\'accomplir sa mission, l\'EOSC doit équiper les chercheurs d\'un réseau européen de dépôts numériques de recherche fiables pour le stockage et l\'analyse des données, avec une répartition disciplinaire et géographique. Le réseau doit offrir la découverte, l\'accès et l\'exploitation fédérés des données, rapprocher les données des services de calcul et d\'analyse, avec les outils pédagogiques et l\'assistance aux utilisateurs correspondants. Cela inclut la mise à disposition de traitement des données sensibles en tant que service et de prestations d\'anonymisation des données afin de garantir que l\'EOSC puisse être un environnement de confiance établissant des normes de sécurité et de protection de haut niveau pour les données de recherche sensibles à caractère privé.

Il est possible de s\'aligner ici sur l\'objectif d\'élargissement aux données du secteur public et de l\'industrie, en convenant d\'un modèle de référence, d\'une architecture, de services et de normes de lacs de données pour un accès et un partage sécurisés de celles-ci. Les entrepôts de données devraient se connecter aux lacs de données en fournissant des données qui peuvent être déployées sur des ressources de calcul et de stockage en tant que paquet pour une analyse ultérieure. Ces lacs devraient être reliés aux services de dépôt afin de fournir l\'infrastructure nécessaire à l\'exploitation des données.

Pour assurer l\'opérationnalisation de l\'EOSC, il sera primordial d'établir un portefeuille d\'accords de fourniture de données avec les principaux fournisseurs de données, tels que les infrastructures de recherche, les collaborations scientifiques et les organismes de recherche, garantissant la fourniture et le soutien des ressources de données essentielles et des outils connexes dans l\'EOSC.

Afin d\'étendre sa capacité de données, l\'EOSC doit faciliter la fédération d\'une quantité croissante de données et de dépôts en fournissant des technologies innovantes offrant des capacités de la fédération EOSC intégrées pour la fourniture à la demande ou en interne de dépôts numériques en tant que service. Les ressources de données fédérées garantissent une collaboration inter-domaines entre les secteurs, y compris l\'industrie, les PME et les décideurs politiques, et doivent s\'intégrer aux espaces de données externes d\'autres secteurs, en s\'alignant sur un modèle commun d'architecture de données et une mise en œuvre de référence des espaces de données.

4.6 Conservation des données à long terme

Comme indiqué dans la charte du groupe de travail sur la conservation à long terme des données de l\'Association EOSC, la possibilité de reproduire, répliquer et réutiliser des résultats scientifiques dépend de l\'accessibilité et de l\'évaluabilité à long terme des données sous-jacentes. Le SRIA souligne l\'importance de la conservation à long terme des données, mais aucune stratégie explicite n\'a encore été formulée pour l\'EOSC.

La conservation numérique est un domaine d\'investissement clé dans les programmes de la CE depuis plusieurs années, mais les activités de ces programmes devraient maintenant être également dirigées vers les données de recherche mises à disposition via l\'EOSC avec des programmes d\'activité spécifiques axés sur les services de conservation numérique alignés sur les dépôts de recherche. La conservation numérique a été un domaine d'investissement clé via l'ESFRI. Ces activités devraient maintenant être reliées à l\'EOSC pour garantir que la préservation à long terme est assurée dans tous les domaines scientifiques, qu\'elle peut être appliquée à de nouveaux types de données et que les données sont accessibles à partir d\'autres domaines afin de faciliter et de promouvoir la réutilisation des données entre les domaines.

Des recherches supplémentaires sont nécessaires pour mettre au point des processus explicites permettant d\'identifier les données, les logiciels et autres produits qui ont une valeur à long terme et devraient être conservés. Pour y parvenir, des travaux développés dans des domaines tels que les sciences de la vie et les sciences sociales peuvent être très utiles. Le développement de telles procédures aidera à adapter les actions de conservation en conséquence dans l\'EOSC à l\'avenir. Une sélection doit être effectuée car les ressources nécessaires pour conserver les données et autres objets de recherche sont substantielles. Ce processus de sélection sera une tâche continue et dynamique, sachant que la décision sur ce qu\'il faut conserver évolue avec le temps. Lorsque l\'on dispose d\'une vue d'ensemble initiale de ce qui doit être conservé, il convient d\'appliquer à ce sous-ensemble de contenu des entrepôts de conservation numérique spécialisés qui contrôlent l\'obsolescence des formats et appliquent des actions de conservation appropriées. Un réseau d'entrepôts de conservation qui peut s\'intégrer à d\'autres infrastructures pour sauvegarder les données et les ressources mises à disposition dans l\'EOSC doit être encouragé.

4.7 Qualité et fiabilité des données et des services

Les données au sein de l\'EOSC doivent être de haute qualité pour être fiables et doivent être conservées afin de pouvoir être référencées et réutilisées à long terme. Les communautés de recherche diffèrent dans les mesures qu\'elles appliquent pour évaluer la qualité des données, mais des caractéristiques et des évaluations communes pourraient être explorées pour permettre de mesurer la confiance de la communauté dans les ressources. Des modèles de certification comme CoreTrustSeal ont été recommandés pour adoption dans l\'EOSC par le rapport du groupe d\'experts[^3]  « Turning FAIR into Reality » et devraient être poursuivis.

Nous devons pouvoir faire confiance aux services qui stockent nos données à long terme, et mesurer la qualité et le caractère FAIR des données elles-mêmes par le biais d\'un examen communautaire et de l\'application d'indicateurs. L\'existence de services certifiés qui garantissent une bonne qualité de mise en œuvre des principes FAIR dans leur domaine d\'expertise pourrait être un moyen d\'assurer une confiance permanente dans la qualité des données et des services de dépôt. Les recommandations ici sont conformes aux priorités existantes signalées par l\'Association EOSC sur la qualité des données de recherche et des dépôts de recherche, mais mettent l\'accent sur l\'aspect confiance.

4.8 Infrastructure de soutien humain

Tous les acteurs doivent disposer des aptitudes et des compétences appropriées pour interagir avec l\'EOSC et accroître l\'utilisation des ressources. Cela doit être fait de manière coordonnée afin de garantir que les communautés techniquement faibles et aucun pays ne soient laissés pour compte, afin d\'uniformiser les règles du jeu. L\'infrastructure humaine qui soutient l\'utilisation de la technologie est essentielle et souvent négligée ou insuffisamment dotée en ressources. L\'objectif de cette action est d\'élargir et de renforcer la base d\'utilisateurs de l\'EOSC en gérant un programme européen coordonné de soutien technique aux utilisateurs. L\'offre d\'un soutien coordonné de « la dernière ligne droite » pour aider les groupes de recherche à mettre en place des services est extrêmement importante pour intégrer les multiples ressources de l\'EOSC dans les environnements d\'utilisateurs actuels et futurs. Cela nécessite une expertise pour intégrer les ressources et les services, impliquant des experts en données et en services des infrastructures de recherche, des e-infrastructures, des nœuds nationaux émergents de l\'EOSC et d\'autres de ses fournisseurs.

Des programmes adaptés seront nécessaires pour travailler avec les utilisateurs de différents groupes, notamment les collaborations de recherche, la longue traîne de la science, la science citoyenne, les entités privées et les décideurs politiques qui se basent sur des éléments concrets. L\'infrastructure de soutien doit intégrer les réseaux humains nationaux participant aux infrastructures électroniques et aux infrastructures de recherche, et développer leur capacité à soutenir les utilisateurs et leurs besoins numériques, à fournir des solutions sur mesure, à développer et à adopter l\'innovation, et à soutenir l\'adoption par les utilisateurs des normes, des produits et des services. Les infrastructures de soutien humain sont déjà disponibles auprès des fournisseurs d\'infrastructures individuels, mais le soutien devra être fourni de manière intégrée et coordonnée pour que l\'EOSC puisse aider à mettre en place des services et offrir un soutien aux utilisateurs qui ont besoin de ces services et qui les combinent à travers les infrastructures individuelles.

Nous ne pouvons pas simplement dresser une liste de services et supposer que les utilisateurs savent ce dont ils ont besoin et comment les combiner. Il est recommandé de faciliter les choses par le biais d\'un service d\'assistance coordonné ou fédéré, de fournisseurs de services de données et d\'intermédiaires tels que les gestionnaires de données et les directeurs de laboratoire qui travaillent en étroite collaboration avec les groupes de recherche et qui pourraient servir de courtier ou de liaison avec les fournisseurs d\'infrastructures de l'EOSC.

Les responsabilités de cet approvisionnement touchent les trois niveaux indiqués dans la feuille de route pluriannuelle : la Commission européenne encourage le soutien humain nécessaire pour s\'engager et interagir avec les services EOSC-Core, tandis que les États membres, les bailleurs de fonds et les institutions nationales sont responsables de la fourniture d\'un soutien local. Des modèles tels que l\'investissement annuel du Flemish Open Science Board (FOSB) dans la science ouverte, dont une partie est consacrée au financement des gestionnaires de données dans les universités, sont recommandés pour accroître la capacité humaine. La connexion entre les couches de soutien locales, nationales et européennes est également essentielle pour promouvoir l\'alignement des programmes d\'études et le partage des compétences.

4.9 Formation et renforcement des capacités

L\'une des principales recommandations du projet de recommandation de l\'UNESCO sur une science ouverte est « d'investir dans les ressources humaines, la formation, l\'éducation, la culture numérique et le renforcement des capacités au service de la science ouverte ». À cette fin, et en complément de la sous-section précédente, il est nécessaire de s\'assurer que les chercheurs disposent des compétences de base pour favoriser un juste équilibre au-delà des frontières et des disciplines. De plus, en relation avec la sous-section sur l\'élargissement et le renforcement de la base d\'utilisateurs de l\'EOSC, il est essentiel pour l\'avenir de l\'EOSC de propager les valeurs fondamentales de la science ouverte et des principes FAIR. Pour toutes ces raisons, une formation de qualité facilement accessible et durable est essentielle.

Les infrastructures sont constituées de personnes, de processus et de technologies qui prennent soin des objets numériques, permettant un accès sans faille aux données, aux publications et aux logiciels. Chaque groupe scientifique déploie des efforts concertés en matière de formation, mais il est possible de créer des supports de formation communs et d\'organiser des événements interdisciplinaires et intergroupes. Il est nécessaire de partager les connaissances entre les communautés afin d\'établir les meilleures pratiques, puis de dispenser une formation pour garantir la conformité et l\'adoption de ces pratiques.

Nous pourrions partir d\'un tronc commun des meilleures pratiques et de matériels et l\'adapter à chaque contexte plutôt que de devoir tout mettre en œuvre à partir de zéro. Cela permettrait également de promouvoir l\'usage de cas d'usage intergroupes, soutenus par des intendants de données et des scientifiques de données.

Il est recommandé de faire bénéficier les projets axés sur le développement d\'activités, de compétences et de meilleures pratiques en matière d\'intendance des données d\'un soutien et d\'un financement adéquats dans le cadre des prochains appels INFRAEOSC en 2023-2024, afin de stimuler et de favoriser la collaboration entre pays et entre disciplines ainsi que l\'alignement des pratiques. La reconnaissance de ces rôles d\'intendance des données et la professionnalisation des parcours professionnels sont également essentielles pour encourager la poursuite du développement.

Un ensemble limité d\'infrastructures de formation de base doit également être soutenu par un financement durable permanent afin de permettre la mise en commun du contenu de la formation et des plateformes d\'apprentissage. Cela pourrait être mis en place sur la base d\'un consortium d\'infrastructures disciplinaires globales plutôt qu\'une activité financée par la CE, en s\'appuyant sur le Knowledge Hub mis en place par EOSC Future.

4.10 Suivi et évaluation de la science ouverte via les graphiques de connaissances scientifiques

La surveillance et le suivi de l\'évolution de la science sont essentiels pour garantir sa transparence, qui conduit à une attribution, une reproductibilité, une évaluation et une appréciation appropriées.

Les graphes de connaissances scientifiques (SKG)[^4] s\'imposent comme un outil flexible pour suivre les faits relatifs à la science, de la publication d\'un article et des données de recherche aux liens sémantiques entre les deux, en passant par les informations contextuelles sur les chercheurs, les organisations, les bailleurs de fonds et les services, afin d\'offrir un graphique interconnecté de l\'activité de recherche. Les SKG peuvent avoir un caractère régional, transdisciplinaire ou disciplinaire, afin de servir les applications pour lesquelles ils ont été conçus. Celles-ci peuvent aller de l\'impact et de l\'évaluation de la recherche à la découverte et à la reproductibilité.

Les domaines d\'activité spécifiques proposés pour le programme de travail 2023-2024 sont les suivants :

\- Construction de SKG spécifiques à un domaine et nationaux (ou régionaux) ;

\- Catalogues et SKG de l\'EOSC ;

\- Interopérabilité des SKG :

Modèles communs : formats de métadonnées convenus, protocoles pour partager les métadonnées sur les entités liées à la recherche et les organisations scientifiques, par exemple les publications, les données, les logiciels, les flux de travail, l\'informatique, le stockage, les organisations, les projets, les financeurs, les services, les chercheurs, les installations, les entreprises ;

Outils communs : intelligence artificielle, extraction de texte intégral et traitement du langage naturel pour les SKG : flux de travail, modèles, logiciels, outils, provenance et reproductibilité.

\- Applications basées sur les SKG :

Surveillance et comptabilité, suivi des événements scientifiques, publication, citation, traitement des données, utilisation des données et des logiciels, consommation de services ;

Services d\'impact sur les SKG : Indicateurs de la science ouverte et bibliométrie, qualité, performance, impact, popularité, etc.

4.11 Profil des chercheurs de l\'EOSC

Cette activité est proposée pour développer des profils pour les chercheurs dans l'EOSC, en les reliant aux identifiants nationaux et institutionnels ou à ORCID, grâce auxquels les chercheurs peuvent se connecter à travers les services (EOSC AAI), suivre et exposer leur carrière scientifique et leurs résultats, et participer à des projets au niveau de la CE et des pays. Le profil peut être utilisé pour demander un financement via le portail des participants de la CE ou, inversement, afin d'être contacté pour une expertise.

Le profil devrait soutenir une infrastructure où des services à valeur ajoutée peuvent être créés (par exemple, des outils pour recevoir des notifications, auxquels les infrastructures nationales ou de recherche peuvent se connecter) afin d\'attribuer des droits de profilage et de soutenir l\'évaluation. Comme le profil d\'ouverture de Knowledge Exchange[^5], le profil de chercheur dans l\'EOSC aidera également à attribuer la paternité et à construire les systèmes de reconnaissance et de récompense qui sont essentiels pour motiver l\'engagement dans la science ouverte et faire appliquer les principes FAIR. Le profil peut offrir un portefeuille des résultats et des activités d\'un chercheur, compilé à travers les différentes identités associées, aidant à reconnaître les contributions et à soutenir des collaborations et un engagement plus larges.

[^1]: https://conosc.org/

[^2]: https://www.lcrdm.nl/dcc

[^3]: https://op.europa.eu/en/publication-detail/-/publication/7769a148-f1f6-11e8-9982-01aa75ed71a1/language-en/format- PDF/source-80611283

[^4]: Pour une définition des graphes de connaissances, voir : https://www.ontotext.com/knowledgehub/fundamentals/what-is-a-knowledge-graph

[^5]: https://www.knowledge-exchange.info/event/openness-profile
