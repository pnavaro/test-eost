# Opinion du groupe de travail EOSC Architecture sur le socle minimum de l'EOSC

![](./media/rId20.jpg){width="2.131817585301837in" height="4.0in"} \[\]\[./media/image2.jpeg)

> Rapport du groupe de travail (WG) Architecture du Bureau exécutif de l'EOSC
>
> Bureau exécutif de l'EOSC WG Architecture Février 2021
>
> **Opinion du groupe de travail EOSC Architecture sur le socle minimum de l'EOSC**
>
> Commission européenne
>
> Direction générale de la recherche et de l'innovation
>
> Directorat G - Unité de sensibilisation à la recherche et à l'innovation Unité G.4 - Science ouverte
>
> Contact Corina Pascu
>
> Courriel <Corina.PASCU@ec.europa.eu>
>
> RTD-EOSC@ec.europa.eu
>
> <RTD-PUBLICATIONS@ec.europa.eu>
>
> Commission européenne
>
> B-1049 Bruxelles
>
> Manuscrit terminé en février 2021.
>
> La Commission européenne n'est pas responsable des conséquences de la réutilisation de cette publication.
>
> Les opinions exprimées dans cette publication relèvent de la seule responsabilité de l'auteur et ne reflètent pas nécessairement celles de la Commission européenne.
>
> De plus amples informations sur l'Union européenne sont disponibles sur l'internet (http://europa.eu).

PDF

ISBN 978-92-76-29813-7

doi : 10.2777/492370

KI-02-21-117-FR-N

> Luxembourg : Office des publications de l'Union européenne, 2021
>
> Union européenne, 2021
>
> La politique de réutilisation des documents de la Commission européenne est mise en œuvre sur la base de la décision 2011/833/UE de la Commission du 12 décembre 2011 relative à la réutilisation des documents de la Commission (JO L 330 du 14.12.2011, p. 39). Sauf indication contraire, la réutilisation de ce document est autorisée sous une licence Creative Commons Attribution 4.0 International (CC-BY 4.0) (https://creativecommons.org/licenses/by/4.0/). Cela signifie que la réutilisation est autorisée à condition que le crédit approprié soit donné et que toute modification soit indiquée.
>
> Pour toute utilisation ou reproduction d'éléments qui ne sont pas la propriété de l'Union européenne, il peut être nécessaire de demander l'autorisation directement aux détenteurs des droits respectifs.
>
> Page de couverture : © Lonely #46246900, ag visuell #16440826, Sean Gladwell #6018533, LwRedStorm #3348265, 2011 ; kras99
>
> #43746830, 2012. Source : Fotolia.com.
>
> COMMISSION EUROPÉENNE
>
> **Opinion du groupe de travail EOSC Architecture sur le socle minimum de l'EOSC**
>
> ***Rapport du Groupe de travail (WG) Architecture du Bureau exécutif de l'EOSC***

Edité par : le Bureau exécutif de l'EOSC

Février 2021

Rédacteurs en chef

Mark van de Sanden (SURF, ORCID : 0000-0002-2718-8918)

Dale Robertson (JISC, ORCID : 0000-0002-7723-0336)

Owen Appleton (EGI, ORCID : 0000-0002-2571-6860)

Paolo Manghi (CNR, ORCID : 0000-0001-7291-3210)

Contributeurs

Membres du WG Architecture de l'EOSC (https://eoscsecretariat.eu/working-groups/architecture-working-group)

> 2021 Direction générale de la recherche et de l'innovation
>
> **Sommaire**

1.  [INTRODUCTION](#Xca92372fede3cd5ebf5341b8159bbd8c1390ef1)......................................................................................................5

2.  ARCHITECTURE DE L'EOSC.....................................................................................8

3.  [FONCTIONS PROPOSÉES POUR L'EOSC-CORE et le MVE](#X4cc80fa07cbe377b7d957a8b40fd05e89e17cf6)...........................................9

4.  [RECOMMANDATIONS](#X4bc0636e31bccc930a9fa18702aebafa400e7d4)..............................................................................................21

> []{#Xca92372fede3cd5ebf5341b8159bbd8c1390ef1 .anchor}**1 INTRODUCTION**

En décembre 2019, le groupe de travail sur la durabilité (ESWG) a publié la version Tinman\[1\] du rapport sur les solutions pour un EOSC durable. Via un processus de consultation ouvert, ce rapport a évolué pour aboutir au rapport FAIR Lady\[2\] qui a été publié le 24 novembre 2020. L'objectif de ce rapport est d'explorer les moyens possibles de pérenniser l'European Open Science Cloud au-delà de la phase initiale qui se termine fin 2020. Pour mieux comprendre les éléments nécessaires à la pérennisation, l'ESWG a décrit une première itération d'un socle minimum de l'EOSC (MVE) dans le document FAIR Lady. Le présent document présente le point de vue du groupe de travail EOSC Architecture sur le MVE. Il utilise la terminologie introduite par l'ESWG, à savoir l'EOSC-Core, l'EOSC-Exchange et le MVE et se concentre sur la période allant jusqu'à la fin de 2023, correspondant à la première phase de l'EOSC telle que définie dans l'agenda stratégique pour la recherche et l'innovation (SRIA)\[3\].

Au cours de la même période que la publication de la version Tinman du rapport, les membres du projet EOSC-hub ont publié une version préliminaire d'un document de prise de position\[4\] de la communauté décrivant le point de vue de celle-ci sur le noyau fédérateur de l'EOSC. En outre, OpenAIRE a publié un document de prise de position\[5\] sur sa vision et ses contributions à l'EOSC.

Alors que tous ces documents traitent des détails du *socle minimum* *de l'EOSC*, les rapports de l'ESWG et d'OpenAIRE définissent ce socle minimum comme un ensemble de structures identifiées pour l'EOSC-Core mais manquent de détails sur les services concrets, et le document de l'EOSC-hub se concentre sur le noyau fédérateur (c'est-à-dire l'EOSC-Core), identifiant les services concrets qui devraient le constituer.

À la suite des discussions, le groupe de travail Architecture a défini l'EOSC-Core, l'EOSC-Exchange, l'EOSC-Federation et le MVE comme suit :

-   **EOSC-Core** : l'ensemble des services nécessaires au fonctionnement de l'EOSC (voir tableau 1).

-   **EOSC-Exchange** : l'ensemble des services de fédération enregistrés auprès de l'EOSC par les infrastructures de recherche (RI) et les clusters pour répondre aux besoins des communautés de recherche et de l'élargissement au grand public et au secteur privé.

-   **EOSC-Federation** : l'ensemble des services scientifiques fournis par les RI et les clusters aux communautés respectives ;

> Le **socle minimum de l'EOSC** est conçu comme un ensemble dynamique de ressources de l'EOSC\[6\] listées ci-dessous :

-   Le sous-ensemble des ressources de l'EOSC nécessaires pour constituer de la valeur ajoutée et les opportunités considérées comme essentielles à fournir par l'EOSC à un moment donné, c'est-à-dire pour permettre la découverte, la création, l'accès et l'analyse de services et de produits de recherche essentiels (par exemple, des publications, des jeux de données, des logiciels) via l'EOSC, ce qui serait impossible sinon ;

-   Le sous-ensemble de composants/services de l'EOSC-Core nécessaires pour exploiter et fournir ces ressources.

La relation entre l'EOSC-Core, l'EOSC-Exchange, l'EOSC-Federation et le MVE en tant que sous-ensemble des ressources de l'EOSC est décrite dans la figure 1.

> Figure 1. Diagramme de l'EOSC décrivant la relation entre l'EOSC-Core, l'EOSC-Exchange, l'EOSC-Federation et le MVE.

L'EAWG a également développé un diagramme structurel décrivant une vue d'ensemble fonctionnelle de l'EOSC et a identifié des axes concrets du cadre d'interopérabilité (EIF) de l'EOSC et des fonctions à inclure dans le MVE, en mettant l'accent sur les fonctions essentielles de l'EOSC-Core.

1.  ARCHITECTURE DE L'EOSC

Pour soutenir la discussion au sein de l'EAWG, un schéma structurel de l'EOSC (voir la figure 2), a été développé. Il fournit une vue d'ensemble fonctionnelle de l'EOSC, en identifiant :

-   Les utilisateurs de l'EOSC côté demande et les fournisseurs de ressources de l'EOSC côté offre.

-   Les fonctions et capacités de l'EOSC-Core

-   Le cadre d'interopérabilité de l'EOSC avec des directives d'interopérabilité pour soutenir l'intégration et la composabilité des ressources parmi les fournisseurs et pour relier les ressources aux fonctions de l'EOSC-Core.

> Figure 2. Schéma structurel illustrant les fonctions de l'EOSC-Core supportant l'EOSC

Cette vue d'ensemble fonctionnelle peut être amenée à se développer au fil du temps, au fur et à mesure de l'évolution de l'EOSC.

1.  []{#X4cc80fa07cbe377b7d957a8b40fd05e89e17cf6 .anchor}FONCTIONS PROPOSÉES POUR L'EOSC-CORE ET LE MVE

Étant donné que le rapport du groupe de travail sur la durabilité, les documents de prise de position d'EOSC-hub et d'OpenAIRE abordent les fonctions de l'EOSC-Core de manière différente, le WG Architecture s'est chargé d'identifier le MVE au niveau des fonctions. Le résultat de cette opération figure dans le tableau 1.

Tous les programmes et fonctions qui sont identifiés dans les documents FAIR Lady, EOSC-hub et OpenAIRE sont inclus dans le tableau. Grâce au consensus obtenu lors des discussions au sein des réunions régulières de l'EAWG, les membres de celui-ci ont établi une proposition de fonctions à inclure dans le MVE, en se concentrant sur les fonctions d'EOSC-Core, et ont assigné des phases temporelles dans lesquelles chaque fonction devrait être activée (voir les notes de fin pour plus de détails). Pour chacune des fonctions, une brève description et des références aux sections correspondantes du rapport FAIR Lady et du SRIA sont fournies.

Dans le tableau 1, les fonctions définies comme appartenant à l'EOSC-Core et prévues dans la phase 1 font partie du MVE initial. Les fonctions de l'EOSC-Core affectées aux phases 2 ou 3 peuvent être mises en œuvre ultérieurement et faire partie d'une prochaine version du MVE. Certaines fonctions du tableau font partie du MVE initial, mais sont considérées comme ne faisant pas partie de l'EOSC-Core. Certaines des fonctions proposées dans le tableau sont considérées par l'EAWG comme ne faisant pas partie du MVE initial.

Domaine

Fonction dans le MVE

ESWG Fair Lady

Proposition de l'EAWG

Phase (1-3)

Description fonctionnelle

Référence au champ d'action SRIA

Services PID

PID pour les services de l'EOSC

1.1

EOSC-Core : PID

L'EOSC doit sélectionner et exécuter un service (faisant partie de l'EOSC-Core) pour fournir des PID aux services de l'EOSC

2

Un moyen de créer/résoudre les PID pour les services dans le paysage de l'EOSC (services de base et services intégrés) afin de permettre une identification unique, de citer, d'éviter les doublons et de suivre l'impact. Il peut s'agir d'un service permettant de générer, de résoudre et de valider les identifiants pérennes (PID) ou d'un service pour relier les systèmes PID existants.

Identifiants du champ d'action (AA)1

PID pour les entités de recherche dans l'EOSC

1.1

EOSC-Core : PID

Le cadre réglementaire des PID doit être défini par l'EOSC

1

Cadre réglementaire des PID pour les entités de recherche (données, logiciels, publications, organisations, chercheurs, financeurs, etc.)

À déterminer par la task force PID du groupe de travail WG-Arch.

Identifiants AA1

Portail, catalogues et commandes

Portail de l'EOSC (site Internet)

1.1

EOSC-Core : portail web

Fait partie de l'EOSC-Core

1

Site Internet contenant les informations de base à propos de l'EOSC et le(s) catalogue(s). Offre les fonctions de base de gestion pour les utilisateurs.

Environnements de l'utilisateur AA5, environnements de fournisseurs de ressources AA6

Catalogue de services du portail de l'EOSC et portefeuille fédéré

1.1

EOSC-Core : portail web

Fait partie de l'EOSC-Core

1

Interface utilisateur du portail qui montre les services aux clients. Elle permet de rechercher, découvrir et commander des services.

Interface d'administration permettant de collecter des listes de services, de les publier sous forme de catalogue et de faire circuler les données sur ces services entre les catalogues thématiques, régionaux et nationaux. Comprend l'embarquement.

Métadonnées et ontologies AA2, Environnement de l'utilisateur AA5 (interface utilisateur) AA6 Environnements des fournisseurs de ressources (interface d'administration)

BC1 RoP

Portefeuille/catalogue de sources de données de l'EOSC et embarquement des sources de données (inscription, validation, découverte)

1.1

EOSC-Core : portail web

Fait partie de l'EOSC-Core

1

L'interface utilisateur du portail qui expose les sources de données aux clients.

Permet l'inscription/l'embarquement,

la recherche, la découverte, la validation/certification des sources de données.

Interface d'administration permettant de collecter des informations sur les sources de données pour l'inscription, soutenir l'embarquement/la validation/la certification par rapport aux lignes directrices (qualité attendue des métadonnées décrivant les produits scientifiques dans les sources de données), et de permettre aux informations sur les sources de données de circuler entre les catalogues de sources de données thématiques, régionaux et nationaux.

Métadonnées et ontologies AA2,

Environnement utilisateur AA5 (interface utilisateur),

Environnements des fournisseurs de ressources (interface d'administration) AA6, BC1 RoP

Portefeuille/catalogue de produits scientifiques du portail de l'EOSC

1.1

EOSC-Core : portail web

Fait partie de l'EOSC-Core

2

Interface d'administration permettant de collecter des listes de produits de recherche, de les publier sous forme de catalogue et de faire circuler les données sur les produits de recherche entre les catalogues thématiques, régionaux et nationaux. Inclut l'embarquement.

Métadonnées et ontologies AA2,

Environnement utilisateur AA5 (interface utilisateur),

Environnements des fournisseurs de ressources (interface d'administration) AA6, BC1 RoP

Outil de gestion des commandes de l'EOSC

1.1 EOSC-Core : cadre de gestion des services et d'accès, 1.2 EOSC-Exchange

Ne fait pas partie du MVE

Plateforme logicielle pour gérer les commandes de services passées via le catalogue principal de l'EOSC (directement à partir du portail destiné aux chercheurs ou potentiellement à partir d'autres catalogues qui affichent des services tirés du catalogue principal). Collecter les demandes des clients/utilisateurs avec les données pertinentes, les transmettre aux fournisseurs via une API, par courriel ou par d'autres moyens. Soutenir la collecte des indicateurs des commandes.

Environnement utilisateur AA5

Services de données

Services de transfert de données de l'EOSC

1.1

EOSC-Core : cadre d'accès aux données

Ne fait pas partie du MVE

Les services de transfert de données permettent de déplacer des fichiers de données de manière asynchrone entre les points de stockage source et destination, ainsi que des mécanismes permettant de garantir une nouvelle tentative automatique en cas d'échec et d'optimiser les performances de traitement pour les fichiers lourds ou un grand nombre de fichiers.

Données de recherche en tant que service

1.1

EOSC-Core : cadre d'accès aux données

Fait partie du MVE

3

Cadre d'accès aux données, dont le rôle principal est de proposer des données en tant que service, permettant aux consommateurs de données de découvrir et de récupérer les données les mieux adaptées à leurs besoins grâce aux interfaces ouvertes. Le modèle architectural conçu dans l'industrie par l'International Data Spaces Association est particulièrement intéressant. Il détaille les différents rôles et aborde la question de la souveraineté des données, dont certaines parties peuvent s'appliquer au partage (ouvert) des données de la recherche.

Cadre d'interopérabilité de l'EOSC AA7

AAI et Sécurité

EOSC AAI en soutien au portefeuille de services de l'EOSC

1.1

EOSC-Core : AAI

Fait partie de l'EOSC-Core

1

Fournit une infrastructure AAI fédérée distribuée qui permet le partage de la connexion et l'accès aux services et aux données à travers l'EOSC.

AAI AA4

Domaine

Fonction dans le MVE

ESWG Fair Lady

Proposition de l'EAWG

Phase (1-3)

Description fonctionnelle

Référence au champ d'action SRIA

EOSC AAI

pour les services de l'EOSC-Core

1.1

EOSC-Core : AAI

Fait partie de l'EOSC‑Core

1

Fournir une infrastructure AAI pour proposer un système d'authentification unique sur la base d'identités fédérées à travers les services de l'EOSC-Core (fédéré).

Politiques de sécurité et fonctions de coordination de la sécurité de l'EOSC

1.1

EOSC-Core : politiques et procédures de sécurité

Fait partie de l'EOSC‑Core

1

La coordination centrale des activités de sécurité permet de s'assurer que les politiques de confiance et d'identité, la sécurité opérationnelle et la maintenance sont compatibles entre tous les partenaires, en fournissant des services de surveillance pour vérifier les failles en matière de sécurité et d'autres problèmes liés à la sécurité dans l'infrastructure : elle garantit que les incidents sont traités rapidement et efficacement, que les politiques communes sont suivies en fournissant des services tels que la surveillance de la sécurité, et en assurant la formation et la diffusion dans le but d'améliorer la réponse aux incidents.

AAI AA4

Centre d'assistance

Centre d'assistance du portail de l'EOSC-Core

1.1

EOSC-Core : services de soutien opérationnel

Fait partie de l'EOSC‑Core

1

Centre d'assistance classique pour couvrir les incidents liés au portail et aux services de base.

EOSC

Centre d'assistance en tant que service

Ne fait pas partie du MVE

A. Cadre permettant de connecter les centres d'assistance existants afin qu'ils puissent se transférer les tickets.

EOSC

Centre d'assistance en tant que service

Ne fait pas partie du MVE

B. Proposer un centre d'assistance aux communautés qui n'en ont pas ou qui ne souhaitent pas en déployer un elles‑mêmes.

Services de soutien

Logiciel de collaboration de l'EOSC-Core

1.1

EOSC-Core : gestion des services et cadre d'accès

Fait partie de l'EOSC-Core (il faut savoir exactement lesquels de ses services et outils sont nécessaires pour l'évaluation de l'EOSC-Core).

1

Plateforme logicielle pour suivre la documentation, les tâches, la communication des opérations de l'EOSC-Core.

Wikis, systèmes de billetterie, listes de diffusion, systèmes d'appel vidéo, etc.

Services de soutien de l'EOSC

1.1

EOSC-Core : services d'appui opérationnel

Fait partie de l'EOSC‑Core (il faut évaluer exactement lesquels de ses services et outils sont nécessaires pour l'évaluation de l'EOSC-Core).

1

Conseil et formation sur la manière d'utiliser et de bénéficier des services de l'EOSC-Core.

Condition limite (BC)4

Compétences et formation

Formation et soutien de l'EOSC sur la science ouverte

1.1

EOSC-Core : soutient le cadre de la politique scientifique ouverte partagée.

Fait partie de l'EOSC‑Core uniquement s'il n'est pas traité de manière adéquate ailleurs (par exemple, s'il est fourni par les ESFRI).

1

Organiser et faire fonctionner une structure distribuée, humaine et hiérarchique, avec les pays et les disciplines au centre, en abordant les aspects techniques, organisationnels et juridiques de l'interopérabilité des données et des services. Parmi les sujets abordés, citons la FAIRitude des produits scientifiques et des services, les droits de propriété intellectuelle, la gestion des données de la recherche, le DMP, le libre accès aux publications, la reproductibilité de la science, etc.

Compétences et formation BC4

Centre d'assistance de l'EOSC sur la science ouverte et outils collaboratifs

1.1

EOSC-Core : services d'appui opérationnel

Fait partie de l'EOSC‑Core uniquement s'il n'est pas traité de manière adéquate ailleurs (par exemple, s'il est fourni par les ESFRI).

1

Centre d'assistance sur la science ouverte, un tableau de bord collaboratif permettant à un réseau d'experts en science ouverte d'être en contact avec les utilisateurs/fournisseurs de l'EOSC; Registre de matériel pédagogique, qui fédère les registres nationaux/thématiques, afin de regrouper les matériaux européens de formation et de soutien à la science ouverte.

Compétences et formation BC4

Surveillance, indicateurs et comptabilité

Suivi des services de l'EOSC

1.1

EOSC-Core :

cadre ouvert des indicateurs

Fait partie de l'EOSC‑Core : la surveillance passive des services est obligatoire. La surveillance active des services est facultative.

Passive : 1 Active : 2

Suivi de l'utilisation des services afin de rendre compte de leur disponibilité de diverses manières.

Comprend une base de données de gestion de configuration des entités qui contribuent à fournir des services de base pour le support : aide, gestion du changement, surveillance et comptabilité. Le service peut être utilisé pour intégrer les services du portefeuille de l'EOSC pour

la composition.

Modèles de financement

BC1 RoP, BC3

Indicateurs de la science ouverte de l'EOSC

1.1

EOSC-Core : cadre d'indicateurs ouvert, cadre de politique scientifique ouvert et partagé.

Le cadre fait partie de l'EOSC-Core. Fournir des données de comptabilité par les services est facultatif (ne fait pas partie du MVE).

1

Pour que la science ouverte gagne du terrain, l'EOSC doit intégrer une infrastructure dédiée au bénéfice scientifique qui rassemble tous les types de données d'utilisation pour tous les types de ressources (citations, événements d'utilisation pour les données, services, logiciels) et offrir des services d'analyse pour soutenir le calcul reproductible/transparent d'indicateurs et fournir des mesures d'appréciation et d'évaluation à l'échelle communautaire.

Environnements des fournisseurs de ressources AA6

, RoP BC1,

Modèles de financement BC3

Comptabilité de l'EOSC

1.1

EOSC-Core : Cadre d'indicateurs ouvert

Le cadre fait partie de l'EOSC-Core.

Fournir des données de comptabilité par les services est facultatif (ne fait pas partie du MVE).

1

Statistiques d'utilisation des services dans l'EOSC- Exchange pour prendre en charge la facturation et la comptabilité des services par le biais, par exemple, de l'accès virtuel. Lorsque le service de comptabilité de l'EOSC est offert aux fournisseurs de services, la base de données de gestion de configuration de l'EOSC est requise.

RoP BC1,

Modèles de financement BC3

Base de données de gestion de configuration pour les services de base

1.1

EOSC-Core : Services d'appui opérationnel

Fait partie de l'EOSC‑Core

1

Base de données des entités qui contribuent à la prestation des services de base de soutien : aide, gestion du changement, suivi et comptabilité.

Cadre d'interopérabilité de l'EOSC

Cadre politique partagé pour la science ouverte

1.1

EOSC-Core : Cadre de politique scientifique ouvert et partagé

Fait partie de l'EOSC‑Core

1

Pour garantir l'ouverture et l'interopérabilité, le respect de la vie privée et la sécurité (statut du droit d'auteur, limites de divulgation, brevets en instance, autres droits de propriété intellectuelle sur les ensembles de données ou les flux de travail, existence de données personnelles, désignation des données comme propriété intellectuelle, etc.)

4.5.2

EOSC-Core

Cadre de métadonnées interopérables

1.1

EOSC-Core : cadre de métadonnées interopérable

Fait partie de l'EOSC‑Core

1

Compréhension minimale et commune des schémas de métadonnées, de la configuration entre les schémas, de leur enregistrement, des ontologies. Profils pour les schémas de métadonnées, les ontologies, les passerelles, ainsi que les outils pour la gestion des métadonnées.

Ce profilage devrait permettre le développement de registres et de systèmes de recommandation dans ce domaine.

4.5.2

EOSC-Core

Cadre de conformité : règles de participation et cadre de description des ressources

1.1

EOSC-Core, mais définit les lignes directrices pour les ressources à inclure dans l'EOSC-Exchange

Fait partie de l'EOSC‑Core

1

Ce n'est pas un service technique, mais des règles de participation initiales définies par le groupe de travail RoP. Les ressources incluses dans l'EOSC-Exchange doivent se conformer aux règles de participation prédéfinies et doivent être décrites conformément au cadre de description des ressources de l'EOSC, par exemple avec des profils de ressources pour les services, les sources de données, les produits de recherche.

4.5.2

EOSC-

Core, AA5 Environnements des fournisseurs de ressources, AA7

Cadre d'interopérabilité de l'EOSC : Organisationnel, Règles de participation

BC1

Cadre d'accès et de gestion des services

1.1

EOSC-Core : cadre d'accès et de gestion des services

Fait partie de l'EOSC‑Core

1

Ce n'est pas un service technique, mais un cadre de gestion qui permet de fournir des services et des rôles opérationnels dans la prestation des services de base et le soutien des services externes. Comprend un ensemble de rôles, de responsabilités, de documents, de politiques et d'autres documents et outils pour soutenir la gestion des

services.

4.5.2

EOSC-Core

Cadre de conformité : politiques d'interopérabilité

1.1

EOSC-Core, mais définit les lignes directrices pour les ressources à inclure dans l'EOSC-Exchange

Fait partie de l'EOSC‑Core

1

Ce n'est pas un service technique, mais un ensemble de politiques, de recommandations et de normes de facto sur la façon dont les services de l'EOSC peuvent être construits et interagir, ce qui a un fort impact sur les services techniques.

4.5.2

EOSC-Core

Cadre de conformité : politiques

1.1

EOSC-Core : cadre d'accès et de gestion des services

Fait partie de l'EOSC-Core

1

Ce n'est pas un service technique, mais une collection de politiques qui doivent être appliquées dans le domaine de l'EOSC pour assurer sa cohérence et sa génération de valeur

4.5.2

EOSC-Core

Domaine

Fonction dans le MVE

ESWG Fair Lady

Proposition de l'EAWG

Phase (1-3)

Description fonctionnelle

Référence au champ d'action SRIA

Écosystème industriel et commercial

Centre d'innovation numérique de l'EOSC

2.5

Pôles d'innovation numérique

plateforme du DIH faisant potentiellement partie du MVE, mais les ressources fournies par le DIH n'en font pas partie

1

Écosystème de start-ups, de PME, de grandes industries, de chercheurs, d'accélérateurs et d'investisseurs qui favorise la création de partenariats pour stimuler l'innovation.

2.3 Stratégie européenne pour les données : action future pour initier la collaboration avec le réseau des centres d'innovation numérique (Digital Innovation Hubs)

Cadre des marchés publics

5

Recommandations d'achats publics avant commercialisation

Le Cadre de marchés publics fait potentiellement partie du MVE, mais les marchés eux-mêmes n'en font pas partie

1

Cadre pour soutenir les activités de groupements de marchés publics pour des ressources commerciales pour les communautés de recherche financées par des fonds publics, par exemple via des partenariats public-privé (PPP) et/ou des achats publics avant commercialisation (PCP).

7.2

Développement de services et de produits innovants

Notes :

-   Phases telles que définies dans le document SRIA : phase 1 : 2021-2023 ; phase 2 : 2024-2025 ; phase 3 : 2026-2027

-   AA : Champs d'action tels que définis dans la section 5 du SRIA

-   BC : conditions aux limites telles que définies dans la section 6 du SRIA

1.  []{#X4bc0636e31bccc930a9fa18702aebafa400e7d4 .anchor}RECOMMANDATIONS

L'EAWG recommande que :

-   Les propositions pour les fonctions initiales du MVE de l'EOSC présentes dans le tableau 1 (c'est-à-dire prévues dans la phase 1) sont adoptées par la gouvernance de l'EOSC. Les premières feuilles de route pluriannuelles incluent des développements pour les mettre en œuvre et des efforts sont faits pour garantir que ces fonctions sont disponibles au profit des utilisateurs, fournisseurs et autres parties prenantes de l'EOSC.

-   La disponibilité de l'ensemble de fonctions initiales du MVE est communiquée clairement dans le cadre de la diffusion des capacités et de la valeur de l'EOSC.

-   Ces fonctions, qui devraient être assurées au cours des phases 2 et 3, sont notées par la nouvelle gouvernance de l'EOSC et sont utilisées pour communiquer la définition des plans de travail pour les années à venir, afin de garantir que ces fonctions soient assurées dans les délais suggérés.

Enfin, l'EAWG recommande que l'Association EOSC constitue un organe chargé de superviser l'établissement, le contenu et l'évolution du MVE. Cet organe devrait inclure des représentants des clusters d'infrastructures numériques et des ESFRI, qui sont parmi les principales parties prenantes du MVE.

Le groupe de travail sur l'architecture de l'EOSC a confié à la task force sur l'infrastructure d'authentification et d'autorisation (AAI TF) la tâche d'établir un écosystème mondial commun pour les infrastructures de contrôle d'identité et d'accès pour l'European Open Science Cloud (EOSC).

Ce document est le livrable final de la task Force AAI de l'EOSC, opérant dans le contexte du Groupe de Travail Architecture de l'EOSC et sera remis à l'Association de l'EOSC.

> *Politique de recherche et d'innovation*

\[1\] 1 <https://www.eoscsecretariat.eu/system/files/solutions_for_a_sustainable_eosc_-_tinman_draft_02dec19.pdf>

\[2\] 2 <https://op.europa.eu/en/publication-detail/-/publication/581d82a4-2ed6-11eb-b27b-01aa75ed71a1>

\[3\] 3 <https://www.eoscsecretariat.eu/sites/default/files/eosc-sria-v09.pdf>

\[4\] 4 <https://eosc-hub.eu/sites/default/files/EOSC%20Federating%20Core%20Community%20Position%20Paper%20v1.1.pdf>

\[5\] 5 https://doi.org/10.5281/zenodo.3475076

\[6\] \> 6 L'EAWG considère le MVE comme quelque chose qui évoluera avec le temps : la première itération du MVE se concentre, comme le dit l'EAWG, sur la mise en œuvre initiale de l'EOSC qui apporte une valeur ajoutée aux utilisateurs au-delà de leur utilisation actuelle des infrastructures. À l'avenir, cependant, une définition plus étendue du MVE pourra être appliquée pour répondre aux exigences des défis scientifiques particuliers du moment.
