# Règles de participation de l\'EOSC

<https://op.europa.eu/en/publication-detail/-/publication/a96d6233-554e-11eb-b59f-01aa75ed71a1/language-en/format-PDF/source-184432576>

Les règles de participation (RoP) de l\'European Open Science Cloud (EOSC) énoncent les normes et la conduite requises des participants à l\'EOSC. Les participants à l\'EOSC acceptent d\'adhérer à ces RoP. L\'adhésion devrait permettre de rallier la confiance envers l'EOSC et les ressources accessibles par son intermédiaire. Les RoP sont basées sur l\'idée de développer la qualité par la transparence et de minimiser le besoin de réglementation. Ces règles elles-mêmes sont d'un niveau global afin d\'être généralement applicables et durables. Le commentaire de chaque règle ci-dessous fournit un niveau de détail supplémentaire.

-L\'EOSC repose sur le principe d\'ouverture

-Les ressources de l\'EOSC sont conformes aux principes FAIR

-Les services de l\'EOSC sont conformes aux lignes directrices de l\'EOSC en matière d\'architecture et d\'interopérabilité

-L\'EOSC se fonde sur des principes de comportement éthique et d\'intégrité de la recherche

-Les utilisateurs de l\'EOSC sont censés contribuer à l\'EOSC

-Les utilisateurs de l\'EOSC adhèrent aux modalités et conditions associées aux ressources qu\'ils utilisent

-Les utilisateurs de l'EOSC citent les ressources qu\'ils utilisent dans leurs travaux

-La participation à l\'EOSC est soumise aux politiques et à la législation en vigueur

**Définitions**[^1]

**Participant** : personne physique ou morale interagissant avec les ressources de l'EOSC.

**Utilisateur** : participant consommant les ressources de l'EOSC.

**Fournisseur** : participant proposant des ressources de l'EOSC à la consommation.

**Ressource** : objet ou procédé numérique tel que des données, métadonnées, publications, logiciels, flux de travail, services et supports de formation.

**Ressource de l\'EOSC :** une ressource enregistrée/trouvable[^2] par l'intermédiaire du portail de l\'EOSC.

[^1]: Inspirées de la version préliminaire du glossaire de l\'EOSC [[: https://docs.google.com/document/d/1rA3XNw-ORrDzBUyZwOolacTOOBpjOaxWmFdg_EEZcpA/edit]{.underline}](https://docs.google.com/document/d/1rA3XNw-ORrDzBUyZwOolacTOOBpjOaxWmFdg_EEZcpA/edit)

[^2]: L'accès à une ressource peut être indirect : par exemple, la ressource peut être répertoriée dans un entrepôt qui est lui-même répertorié dans un entrepôt accessible par le portail de l'EOSC. Cette récursion peut se faire en plusieurs étapes.
